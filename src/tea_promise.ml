let cmd promise tagger =
  ((let open Vdom in
    Tea_cmd.call (fun callbacks ->
        (let _ =
           promise
           |> Js.Promise.then_ (fun res ->
                  match tagger res with
                  | Some msg ->
                      let () = callbacks.contents.enqueue msg in
                      Js.Promise.resolve ()
                  | None ->
                      Js.Promise.resolve () )
         in
         () )
        [@ns.braces] ) ) [@ns.braces] )

let result promise msg =
  ((let open Vdom in
    Tea_cmd.call (fun callbacks ->
        (let enq result = callbacks.contents.enqueue (msg result) in
         let _ =
           promise
           |> Js.Promise.then_ (fun res ->
                  (let resolve = enq (Ok res) in
                   Js.Promise.resolve resolve )
                  [@ns.braces] )
           |> Js.Promise.catch (fun err ->
                  (let err_to_string err = {j|$err|j} in
                   let reject = enq (Error (err_to_string err)) in
                   Js.Promise.resolve reject )
                  [@ns.braces] )
         in
         () )
        [@ns.braces] ) ) [@ns.braces] )
