val renderEvent : ?key:(string[@ns.namedArgLoc]) -> 'msg -> 'msg Tea_sub.t

module LocalStorage : sig
  val length : (int, string) Tea_task.t

  val clear : (unit, string) Tea_task.t

  val clearCmd : unit -> 'msg Tea_cmd.t

  val key : int -> (string option, string) Tea_task.t

  val getItem : string -> (string option, string) Tea_task.t

  val removeItem : string -> (unit, string) Tea_task.t

  val removeItemCmd : string -> 'msg Tea_cmd.t

  val setItem : string -> string -> (unit, string) Tea_task.t

  val setItemCmd : string -> string -> 'msg Tea_cmd.t
end
