type nonrec ('flags, 'model, 'msg) program =
  { init: 'flags -> 'model * 'msg Tea_cmd.t
  ; update: 'model -> 'msg -> 'model * 'msg Tea_cmd.t
  ; view: 'model -> 'msg Vdom.t
  ; subscriptions: 'model -> 'msg Tea_sub.t
  ; shutdown: 'model -> 'msg Tea_cmd.t }

type nonrec ('flags, 'model, 'msg) standardProgram =
  { init: 'flags -> 'model * 'msg Tea_cmd.t
  ; update: 'model -> 'msg -> 'model * 'msg Tea_cmd.t
  ; view: 'model -> 'msg Vdom.t
  ; subscriptions: 'model -> 'msg Tea_sub.t }

type nonrec ('model, 'msg) beginnerProgram =
  {model: 'model; update: 'model -> 'msg -> 'model; view: 'model -> 'msg Vdom.t}

type nonrec ('model, 'msg) pumpInterface =
  { startup: unit -> unit
  ; renderString: 'model -> string
  ; handleMsg: 'model -> 'msg -> 'model
  ; shutdown: 'msg Tea_cmd.t -> unit }

type nonrec 'msg programInterface =
  < getHtmlString: unit -> string
  ; pushMsg: 'msg -> unit
  ; shutdown: unit -> unit >
  Js.t

external makeProgramInterface :
     pushMsg:(('msg -> unit)[@ns.namedArgLoc])
  -> shutdown:((unit -> unit)[@ns.namedArgLoc])
  -> getHtmlString:((unit -> string)[@ns.namedArgLoc])
  -> 'msg programInterface = ""
[@@obj]

val programStateWrapper :
     'msg
  -> ('msg Vdom.applicationCallbacks ref -> ('msg, 'msg) pumpInterface)
  -> ('msg -> 'msg Tea_cmd.t)
  -> 'msg programInterface

val programLoop :
     ('msg -> 'b -> 'msg * 'b Tea_cmd.t)
  -> ('msg -> 'b Vdom.t)
  -> ('msg -> 'b Tea_sub.t)
  -> 'msg
  -> 'b Tea_cmd.t
  -> Dom.node option
  -> 'b Tea_cmd.applicationCallbacks ref
  -> ('msg, 'b) pumpInterface

val program :
     ('flags, 'model, 'msg) program
  -> Dom.node Js.null_undefined
  -> 'flags
  -> 'msg programInterface

val standardProgram :
     ('flags, 'model, 'msg) standardProgram
  -> Dom.node Js.null_undefined
  -> 'flags
  -> 'msg programInterface

val beginnerProgram :
     ('model, 'msg) beginnerProgram
  -> Dom.node Js.null_undefined
  -> unit
  -> 'msg programInterface

val map : ('msg -> 'b) -> 'msg Vdom.t -> 'b Vdom.t
