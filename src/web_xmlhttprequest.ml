type nonrec unresolved

type nonrec xmlHttpRequestUpload

type nonrec eventReadystatechange = Js.Json.t

type nonrec eventAbort = Js.Json.t

type nonrec eventError = Js.Json.t

type nonrec eventLoad = Js.Json.t

type nonrec eventLoadstart = Js.Json.t

type nonrec eventProgress = Js.Json.t

type nonrec eventTimeout = Js.Json.t

type nonrec eventLoadend = Js.Json.t

type nonrec _xmlhttprequest =
  < onreadystatechange: eventReadystatechange -> unit [@get] [@set]
  ; readyState: int [@get]
  ; responseType: string [@get] [@set]
  ; response: unresolved Js.null
  ; responseText: string [@get]
  ; responseURL: string [@get]
  ; responseXML: Dom.document Js.null [@get]
  ; status: int [@get]
  ; statusText: string [@get]
  ; timeout: float [@get] [@set]
  ; upload: xmlHttpRequestUpload [@get]
  ; withCredentials: bool [@get] [@set]
  ; onabort: eventAbort -> unit [@get] [@set]
  ; onerror: eventError -> unit [@get] [@set]
  ; onload: eventLoad -> unit [@get] [@set]
  ; onloadstart: eventLoadstart -> unit [@get] [@set]
  ; onprogress: eventProgress -> unit [@get] [@set]
  ; ontimeout: eventTimeout -> unit [@get] [@set]
  ; onloadend: eventLoadend -> unit [@get] [@set] >
  Js.t
[@@obj]

type nonrec t = _xmlhttprequest

external abort : t -> unit = "abort" [@@send]

external getAllResponseHeaders : t -> string Js.null = "getAllResponseHeaders"
[@@send]

external getResponseHeader : t -> string Js.null = "getResponseHeader" [@@send]

external open' : t -> string -> string -> bool -> string -> string -> unit
  = "open"
[@@send]

external overrideMimeType : t -> string -> unit = "overrideMimeType" [@@send]

external send : t -> unit = "send" [@@send]

external sendString : t -> string Js.null -> unit = "send" [@@send]

external sendFormData : t -> Webapi.FormData.t -> unit = "send" [@@send]

external sendDocument : t -> Dom.document -> unit = "send" [@@send]

external setRequestHeader : t -> string -> string -> unit = "setRequestHeader"
[@@send]

external create : unit -> t = "XMLHttpRequest" [@@new]

type nonrec errors = IncompleteResponse | NetworkError

type nonrec body =
  | EmptyBody
  | EmptyStringBody
  | StringBody of string
  | FormDataBody of Webapi.FormData.t
  | FormListBody of (string * string) list
  | DocumentBody of Dom.document

let abort (x : t) : unit = x |. abort

let getAllResponseHeaders (x : t) : (string, errors) result =
  match Js.Null.toOption (x |. getAllResponseHeaders) with
  | None ->
      Error IncompleteResponse
  | Some "" ->
      Error NetworkError
  | Some s ->
      Ok s

let getAllResponseHeadersAsList (x : t) :
    ((string * string) list, errors) result =
  match getAllResponseHeaders x with
  | Error _ as err ->
      err
  | Ok s ->
      Ok
        ( s |> Js.String.split "\r\n"
        |> Array.map (Js.String.splitAtMost ": " ~limit:(2 [@ns.namedArgLoc]))
        |> Array.to_list
        |> List.filter (fun a -> Array.length a == 2)
        |> List.map (fun x ->
               match x with
               | [|key; value|] ->
                   (key, value)
               | _ ->
                   failwith "Cannot happen, already checked length" ) )

let getAllResponseHeadersAsDict (x : t) :
    (string Belt.Map.String.t, errors) result =
  (let module StringMap = Belt.Map.String in
  match getAllResponseHeadersAsList x with
  | Error _ as err ->
      err
  | Ok l ->
      let insert d (k, v) = StringMap.set d k v in
      Ok (List.fold_left insert StringMap.empty l) )
  [@ns.braces]

let open' (method' : string) (url : string)
    ?async:((async [@ns.namedArgLoc]) = true)
    ?user:((user [@ns.namedArgLoc]) = "")
    ?password:((password [@ns.namedArgLoc]) = "") (x : t) =
  x |. open' method' url async user password

let overrideMimeType (mimetype : string) (x : t) : unit =
  overrideMimeType x mimetype

let send (body : body) (x : t) : unit =
  match body with
  | EmptyBody ->
      x |. send
  | EmptyStringBody ->
      x |. sendString Js.Null.empty
  | StringBody s ->
      x |. sendString (Js.Null.return s)
  | FormDataBody f ->
      x |. sendFormData f
  | FormListBody l ->
      let form =
        List.fold_left
          (fun f (key, value) ->
            (let () = Webapi.FormData.append key value f in
             f )
            [@ns.braces] )
          (Webapi.FormData.make ()) l
      in
      x |. sendFormData form
  | DocumentBody d ->
      x |. sendDocument d

let setRequestHeader (header : string) (value : string) (x : t) =
  x |. setRequestHeader header value

type nonrec state = Unsent | Opened | HeadersReceived | Loading | Done

type nonrec responseType =
  | StringResponseType
  | ArrayBufferResponseType
  | BlobResponseType
  | DocumentResponseType
  | JsonResponseType
  | TextResponseType
  | RawResponseType of string

type nonrec responseBody =
  | NoResponse
  | StringResponse of string
  | ArrayBufferResponse of unit
  | BlobResponse of unit
  | DocumentResponse of Dom.document
  | JsonResponse of Js.Json.t
  | TextResponse of string
  | RawResponse of string * unit

let set_onreadystatechange (cb : eventReadystatechange -> unit) (x : t) : unit =
  x ## onreadystatechange #= cb

let get_onreadystatechange (x : t) : eventReadystatechange -> unit =
  x##onreadystatechange

let readyState (x : t) : state =
  match x##readyState with
  | 0 ->
      Unsent
  | 1 ->
      Opened
  | 2 ->
      HeadersReceived
  | 3 ->
      Loading
  | 4 ->
      Done
  | i ->
      failwith ("Invalid return from 'readystate' of: " ^ Belt.Int.toString i)

let set_responseType (typ : responseType) (x : t) : unit =
  match typ with
  | StringResponseType ->
      x ## responseType #= ""
  | ArrayBufferResponseType ->
      x ## responseType #= "arraybuffer"
  | BlobResponseType ->
      x ## responseType #= "blob"
  | DocumentResponseType ->
      x ## responseType #= "document"
  | JsonResponseType ->
      x ## responseType #= "json"
  | TextResponseType ->
      x ## responseType #= "text"
  | RawResponseType s ->
      x ## responseType #= s

let get_responseType (x : t) : responseType =
  match x##responseType with
  | "" ->
      StringResponseType
  | "arraybuffer" ->
      ArrayBufferResponseType
  | "blob" ->
      BlobResponseType
  | "document" ->
      DocumentResponseType
  | "json" ->
      JsonResponseType
  | "text" ->
      TextResponseType
  | s ->
      RawResponseType s

let get_response (x : t) : responseBody =
  match Js.Null.toOption x##response with
  | None ->
      NoResponse
  | Some resp -> (
    match get_responseType x with
    | StringResponseType ->
        StringResponse (Obj.magic resp)
    | ArrayBufferResponseType ->
        ArrayBufferResponse (Obj.magic resp)
    | BlobResponseType ->
        BlobResponse (Obj.magic resp)
    | DocumentResponseType ->
        DocumentResponse (Obj.magic resp)
    | JsonResponseType ->
        JsonResponse (Obj.magic resp)
    | TextResponseType ->
        TextResponse (Obj.magic resp)
    | RawResponseType s ->
        RawResponse (s, Obj.magic resp) )

let get_responseText (x : t) : string = x##responseText

let get_responseURL (x : t) : string = x##responseURL

let get_responseXML (x : t) : Dom.document option =
  Js.Null.toOption x##responseXML

let get_status (x : t) : int = x##status

let get_statusText (x : t) : string = x##statusText

let set_timeout (t : float) (x : t) : unit = x ## timeout #= t

let get_timeout (x : t) : float = x##timeout

let set_withCredentials (b : bool) (x : t) : unit = x ## withCredentials #= b

let get_withCredentials (x : t) : bool = x##withCredentials

let set_onabort (cb : eventAbort -> unit) (x : t) : unit = x ## onabort #= cb

let get_onabort (x : t) : eventAbort -> unit = x##onabort

let set_onerror (cb : eventError -> unit) (x : t) : unit = x ## onerror #= cb

let get_onerror (x : t) : eventError -> unit = x##onerror

let set_onload (cb : eventLoad -> unit) (x : t) : unit = x ## onload #= cb

let get_onload (x : t) : eventLoad -> unit = x##onload

let set_onloadstart (cb : eventLoadstart -> unit) (x : t) : unit =
  x ## onloadstart #= cb

let get_onloadstart (x : t) : eventLoadstart -> unit = x##onloadstart

let set_onprogress (cb : eventLoadstart -> unit) (x : t) : unit =
  x ## onprogress #= cb

let get_onprogress (x : t) : eventLoadstart -> unit = x##onprogress

let set_ontimeout (cb : eventTimeout -> unit) (x : t) : unit =
  x ## ontimeout #= cb

let get_ontimeout (x : t) : eventTimeout -> unit = x##ontimeout

let set_onloadend (cb : eventLoadend -> unit) (x : t) : unit =
  x ## onloadend #= cb

let get_onloadend (x : t) : eventLoadend -> unit = x##onloadend
