type nonrec ('flags, 'model, 'msg) program =
  { init: 'flags -> 'model * 'msg Tea_cmd.t
  ; update: 'model -> 'msg -> 'model * 'msg Tea_cmd.t
  ; view: 'model -> 'msg Vdom.t
  ; subscriptions: 'model -> 'msg Tea_sub.t
  ; shutdown: 'model -> 'msg Tea_cmd.t }

type nonrec ('flags, 'model, 'msg) standardProgram =
  { init: 'flags -> 'model * 'msg Tea_cmd.t
  ; update: 'model -> 'msg -> 'model * 'msg Tea_cmd.t
  ; view: 'model -> 'msg Vdom.t
  ; subscriptions: 'model -> 'msg Tea_sub.t }

type nonrec ('model, 'msg) beginnerProgram =
  {model: 'model; update: 'model -> 'msg -> 'model; view: 'model -> 'msg Vdom.t}

type nonrec ('model, 'msg) pumpInterface =
  { startup: unit -> unit
  ; renderString: 'model -> string
  ; handleMsg: 'model -> 'msg -> 'model
  ; shutdown: 'msg Tea_cmd.t -> unit }

type nonrec 'msg programInterface =
  < pushMsg: 'msg -> unit
  ; shutdown: unit -> unit
  ; getHtmlString: unit -> string >
  Js.t

external makeProgramInterface :
     pushMsg:(('msg -> unit)[@ns.namedArgLoc])
  -> shutdown:((unit -> unit)[@ns.namedArgLoc])
  -> getHtmlString:((unit -> string)[@ns.namedArgLoc])
  -> 'msg programInterface = ""
[@@obj]

let programStateWrapper initModel pump shutdown =
  ((let open Vdom in
    let model = ref initModel in
    let callbacks =
      ref
        { enqueue= (fun _msg -> Js.log "INVALID enqueue CALL!")
        ; on= (fun _ -> ()) }
    in
    let pumperInterfaceC () = pump callbacks in
    let pumperInterface = pumperInterfaceC () in
    let (pending : 'msg list option ref) = (ref None : 'msg list option ref) in
    let rec handler msg =
      match pending.contents with
      | None -> (
          let () = pending := Some [] in
          let newModel = pumperInterface.handleMsg model.contents msg in
          let () = model := newModel in
          match pending.contents with
          | None ->
              failwith
                "INVALID message queue state, should never be None during \
                 message processing!"
          | Some [] ->
              pending := None
          | Some msgs ->
              let () = pending := None in
              List.iter handler (List.rev msgs) )
      | Some msgs ->
          pending := Some (msg :: msgs)
    in
    let renderEvents = ref [] in
    let (finalizedCBs : 'msg Vdom.applicationCallbacks) =
      ( { enqueue= (fun msg -> handler msg)
        ; on=
            (fun x ->
              match x with
              | Render ->
                  List.iter handler renderEvents.contents
              | AddRenderMsg msg ->
                  renderEvents := List.append renderEvents.contents [msg]
              | RemoveRenderMsg msg ->
                  renderEvents :=
                    List.filter (fun mg -> msg != mg) renderEvents.contents ) }
        : 'msg Vdom.applicationCallbacks )
    in
    let () = callbacks := finalizedCBs in
    let piRequestShutdown () =
      (let () =
         callbacks :=
           { enqueue=
               (fun _msg -> Js.log "INVALID message enqueued when shut down")
           ; on= (fun _ -> ()) }
       in
       let cmd = shutdown model.contents in
       let () = pumperInterface.shutdown cmd in
       () )
      [@ns.braces]
    in
    let renderString () =
      (let rendered = pumperInterface.renderString model.contents in
       rendered )
      [@ns.braces]
    in
    let () = pumperInterface.startup () in
    makeProgramInterface ~pushMsg:(handler [@ns.namedArgLoc])
      ~shutdown:(piRequestShutdown [@ns.namedArgLoc])
      ~getHtmlString:(renderString [@ns.namedArgLoc]) ) [@ns.braces] )

let programLoop update view subscriptions initModel initCmd x =
  match x with
  | None ->
      fun callbacks ->
        (let oldSub = ref Tea_sub.none in
         let handleSubscriptionChange model =
           (let newSub = subscriptions model in
            oldSub := Tea_sub.run callbacks callbacks oldSub.contents newSub )
           [@ns.braces]
         in
         { startup=
             (fun () ->
               (let () = Tea_cmd.run callbacks initCmd in
                let () = handleSubscriptionChange initModel in
                () )
               [@ns.braces] )
         ; renderString=
             (fun model ->
               (let vdom = view model in
                let rendered = Vdom.renderToHtmlString vdom in
                rendered )
               [@ns.braces] )
         ; handleMsg=
             (fun model msg ->
               (let newModel, cmd = update model msg in
                let () = Tea_cmd.run callbacks cmd in
                let () = handleSubscriptionChange newModel in
                newModel )
               [@ns.braces] )
         ; shutdown=
             (fun cmd ->
               (let () = Tea_cmd.run callbacks cmd in
                let () =
                  oldSub :=
                    Tea_sub.run callbacks callbacks oldSub.contents Tea_sub.none
                in
                () )
               [@ns.braces] ) } )
        [@ns.braces]
  | Some parentNode ->
      fun callbacks ->
        (let priorRenderedVdom = ref [] in
         let latestModel = ref initModel in
         let noFrameID = Some (Obj.magic (-1)) in
         let (nextFrameID : Webapi.rafId option ref) = ref None in
         let doRender _delta =
           match nextFrameID.contents with
           | None ->
               ()
           | Some _id ->
               let newVdom = [view latestModel.contents] in
               let justRenderedVdom =
                 Vdom.patchVNodesIntoElement callbacks parentNode
                   priorRenderedVdom.contents newVdom
               in
               let () = priorRenderedVdom := justRenderedVdom in
               let () = callbacks.contents.on Render in
               nextFrameID := None
         in
         let scheduleRender () =
           match nextFrameID.contents with
           | Some _ ->
               ()
           | None ->
               let realtimeRendering = false in
               if realtimeRendering then
                 let () = nextFrameID := noFrameID in
                 doRender 16
               else
                 let id = Webapi.requestCancellableAnimationFrame doRender in
                 let () = nextFrameID := Some id in
                 ()
         in
         let clearPnode () =
           while
             Webapi.Dom.NodeList.length (Webapi.Dom.Node.childNodes parentNode)
             > 0
           do
             match Webapi.Dom.Node.firstChild parentNode with
             | None ->
                 ()
             | Some firstChild ->
                 let _removedChild =
                   Webapi.Dom.Node.removeChild (firstChild [@ns.namedArgLoc])
                     parentNode
                 in
                 ()
           done
         in
         let oldSub = ref Tea_sub.none in
         let handleSubscriptionChange model =
           (let newSub = subscriptions model in
            oldSub := Tea_sub.run callbacks callbacks oldSub.contents newSub )
           [@ns.braces]
         in
         let handlerStartup () =
           (let () = clearPnode () in
            let () = Tea_cmd.run callbacks initCmd in
            let () = handleSubscriptionChange latestModel.contents in
            let () = nextFrameID := noFrameID in
            let () = doRender 16 in
            () )
           [@ns.braces]
         in
         let renderString model =
           (let vdom = view model in
            let rendered = Vdom.renderToHtmlString vdom in
            rendered )
           [@ns.braces]
         in
         let handler model msg =
           (let newModel, cmd = update model msg in
            let () = latestModel := newModel in
            let () = Tea_cmd.run callbacks cmd in
            let () = scheduleRender () in
            let () = handleSubscriptionChange newModel in
            newModel )
           [@ns.braces]
         in
         let handlerShutdown cmd =
           (let () = nextFrameID := None in
            let () = Tea_cmd.run callbacks cmd in
            let () =
              oldSub :=
                Tea_sub.run callbacks callbacks oldSub.contents Tea_sub.none
            in
            let () = priorRenderedVdom := [] in
            let () = clearPnode () in
            () )
           [@ns.braces]
         in
         { startup= handlerStartup
         ; renderString
         ; handleMsg= handler
         ; shutdown= handlerShutdown } )
        [@ns.braces]

let (program :
         ('flags, 'model, 'msg) program
      -> Dom.node Js.null_undefined
      -> 'flags
      -> 'msg programInterface ) =
  ( fun {init; update; view; subscriptions; shutdown} pnode flags ->
      (let initModel, initCmd = init flags in
       let opnode = Js.Nullable.toOption pnode in
       let pumpInterface =
         programLoop update view subscriptions initModel initCmd opnode
       in
       programStateWrapper initModel pumpInterface shutdown )
      [@ns.braces]
    :    ('flags, 'model, 'msg) program
      -> Dom.node Js.null_undefined
      -> 'flags
      -> 'msg programInterface )

let (standardProgram :
         ('flags, 'model, 'msg) standardProgram
      -> Dom.node Js.null_undefined
      -> 'flags
      -> 'msg programInterface ) =
  ( fun {init; update; view; subscriptions} pnode args ->
      program
        { init
        ; update
        ; view
        ; subscriptions
        ; shutdown= (fun _model -> Tea_cmd.none) }
        pnode args
    :    ('flags, 'model, 'msg) standardProgram
      -> Dom.node Js.null_undefined
      -> 'flags
      -> 'msg programInterface )

let (beginnerProgram :
         ('model, 'msg) beginnerProgram
      -> Dom.node Js.null_undefined
      -> unit
      -> 'msg programInterface ) =
  ( fun {model; update; view} pnode () ->
      standardProgram
        { init= (fun () -> (model, Tea_cmd.none))
        ; update= (fun model msg -> (update model msg, Tea_cmd.none))
        ; view
        ; subscriptions= (fun _model -> Tea_sub.none) }
        pnode ()
    :    ('model, 'msg) beginnerProgram
      -> Dom.node Js.null_undefined
      -> unit
      -> 'msg programInterface )

let map func vnode = Vdom.map func vnode
