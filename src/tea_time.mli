type nonrec t = float

val every :
  key:(string[@ns.namedArgLoc]) -> float -> (float -> 'msg) -> 'msg Tea_sub.t

val delay : float -> 'msg -> 'msg Tea_cmd.t

val millisecond : float

val second : float

val minute : float

val hour : float

val inMilliseconds : 'msg -> 'msg

val inSeconds : float -> float

val inMinutes : float -> float

val inHours : float -> float
