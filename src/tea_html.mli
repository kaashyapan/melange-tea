val map : ('msg -> 'b) -> 'msg Vdom.t -> 'b Vdom.t

val text : string -> 'msg Vdom.t

val node :
     ?namespace:(string[@ns.namedArgLoc])
  -> string
  -> ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val noNode : 'msg Vdom.t

val lazy1 : string -> (unit -> 'msg Vdom.t) -> 'msg Vdom.t

val h1 :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val h2 :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val h3 :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val h4 :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val h5 :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val h6 :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val div :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val p :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val hr :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val pre :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val blockquote :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val span :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val a :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val code :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val em :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val strong :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val i :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val b :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val u :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val sub :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val sup :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val br : 'msg Vdom.properties -> 'msg Vdom.t

val br' :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val ol :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val ul :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val li :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val dl :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val dt :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val dd :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val img :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val iframe :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val canvas :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val math :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val form :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val input' :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val textarea :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val button :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val select :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val option :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val optgroup :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val label :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fieldset :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val legend :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val section :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val nav :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val article :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val aside :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val header :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val footer :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val address :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val main :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val body :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val figure :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val figcaption :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val table :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val caption :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val colgroup :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val col :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val tbody :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val thead :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val tfoot :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val tr :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val th :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val td :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val datalist :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val keygen :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val output :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val progress :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val meter :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val audio :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val video :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val source :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val track :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val embed :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val object' :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val param :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val ins :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val del :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val small :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val cite :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val dfn :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val abbr :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val time :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val var :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val samp :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val kbd :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val s :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val q :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val mark :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val ruby :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val rt :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val rp :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val bdi :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val bdo :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val wbr :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val details :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val summary :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val menuitem :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val menu :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val meta :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t

val style :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> string
  -> 'msg Vdom.t

val title :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> string
  -> 'msg Vdom.t

val link :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t

module Attributes : sig
  val noProp : 'msg Vdom.property

  val style : string -> string -> 'msg Vdom.property

  val styles : (string * string) list -> 'msg Vdom.property

  val class' : string -> 'msg Vdom.property

  val classList : (string * bool) list -> 'msg Vdom.property

  val id : string -> 'msg Vdom.property

  val title : string -> 'msg Vdom.property

  val hidden : bool -> 'msg Vdom.property

  val type' : string -> 'msg Vdom.property

  val value : string -> 'msg Vdom.property

  val defaultValue : string -> 'msg Vdom.property

  val checked : bool -> 'msg Vdom.property

  val placeholder : string -> 'msg Vdom.property

  val selected : bool -> 'msg Vdom.property

  val accept : string -> 'msg Vdom.property

  val acceptCharset : string -> 'msg Vdom.property

  val action : string -> 'msg Vdom.property

  val autocomplete : bool -> 'msg Vdom.property

  val autofocus : bool -> 'msg Vdom.property

  val disabled : bool -> 'msg Vdom.property

  val enctype : string -> 'msg Vdom.property

  val formaction : string -> 'msg Vdom.property

  val list : string -> 'msg Vdom.property

  val minlength : int -> 'msg Vdom.property

  val maxlength : int -> 'msg Vdom.property

  val method' : string -> 'msg Vdom.property

  val multiple : bool -> 'msg Vdom.property

  val name : string -> 'msg Vdom.property

  val novalidate : bool -> 'msg Vdom.property

  val pattern : string -> 'msg Vdom.property

  val readonly : bool -> 'msg Vdom.property

  val required : bool -> 'msg Vdom.property

  val size : int -> 'msg Vdom.property

  val for' : string -> 'msg Vdom.property

  val form : string -> 'msg Vdom.property

  val max : string -> 'msg Vdom.property

  val min : string -> 'msg Vdom.property

  val step : string -> 'msg Vdom.property

  val cols : int -> 'msg Vdom.property

  val rows : int -> 'msg Vdom.property

  val wrap : string -> 'msg Vdom.property

  val href : string -> 'msg Vdom.property

  val target : string -> 'msg Vdom.property

  val download : bool -> 'msg Vdom.property

  val downloadAs : string -> 'msg Vdom.property

  val hreflang : string -> 'msg Vdom.property

  val media : string -> 'msg Vdom.property

  val ping : string -> 'msg Vdom.property

  val rel : string -> 'msg Vdom.property

  val ismap : bool -> 'msg Vdom.property

  val usemap : string -> 'msg Vdom.property

  val shape : string -> 'msg Vdom.property

  val coords : string -> 'msg Vdom.property

  val src : string -> 'msg Vdom.property

  val height : int -> 'msg Vdom.property

  val width : int -> 'msg Vdom.property

  val alt : string -> 'msg Vdom.property

  val autoplay : bool -> 'msg Vdom.property

  val controls : bool -> 'msg Vdom.property

  val loop : bool -> 'msg Vdom.property

  val preload : string -> 'msg Vdom.property

  val poster : string -> 'msg Vdom.property

  val default : bool -> 'msg Vdom.property

  val kind : string -> 'msg Vdom.property

  val srclang : string -> 'msg Vdom.property

  val sandbox : string -> 'msg Vdom.property

  val seamless : bool -> 'msg Vdom.property

  val srcdoc : string -> 'msg Vdom.property

  val reversed : bool -> 'msg Vdom.property

  val start : int -> 'msg Vdom.property

  val colspan : int -> 'msg Vdom.property

  val rowspan : int -> 'msg Vdom.property

  val headers : string -> 'msg Vdom.property

  val scope : string -> 'msg Vdom.property

  val align : string -> 'msg Vdom.property

  val async : bool -> 'msg Vdom.property

  val charset : string -> 'msg Vdom.property

  val content : string -> 'msg Vdom.property

  val defer : bool -> 'msg Vdom.property

  val httpEquiv : string -> 'msg Vdom.property

  val language : string -> 'msg Vdom.property

  val scoped : string -> 'msg Vdom.property

  val accesskey : char -> 'msg Vdom.property

  val contenteditable : bool -> 'msg Vdom.property

  val contextmenu : string -> 'msg Vdom.property

  val dir : string -> 'msg Vdom.property

  val draggable : string -> 'msg Vdom.property

  val dropzone : string -> 'msg Vdom.property

  val itemprop : string -> 'msg Vdom.property

  val lang : string -> 'msg Vdom.property

  val spellcheck : bool -> 'msg Vdom.property

  val tabindex : int -> 'msg Vdom.property

  val challenge : string -> 'msg Vdom.property

  val keytype : string -> 'msg Vdom.property

  val cite : string -> 'msg Vdom.property

  val datetime : string -> 'msg Vdom.property

  val pubdate : string -> 'msg Vdom.property

  val manifest : string -> 'msg Vdom.property

  val role : string -> 'msg Vdom.property

  val ariaChecked : bool -> 'msg Vdom.property

  val ariaHidden : bool -> 'msg Vdom.property
end

module Events : sig
  type nonrec options = {stopPropagation: bool; preventDefault: bool}

  val onCB :
       key:(string[@ns.namedArgLoc])
    -> string
    -> (Dom.event -> 'msg option)
    -> 'msg Vdom.property

  val onMsg : string -> 'msg -> 'msg Vdom.property

  val on :
       key:(string[@ns.namedArgLoc])
    -> string
    -> ('event, 'msg) Tea_json.Decoder.t
    -> 'msg Vdom.property

  val onWithOptions :
       key:(string[@ns.namedArgLoc])
    -> string
    -> options
    -> ('event, 'msg) Tea_json.Decoder.t
    -> 'msg Vdom.property

  val defaultOptions : options

  val targetValue : (Js.Json.t, string) Tea_json.Decoder.t

  val targetChecked : (Js.Json.t, bool) Tea_json.Decoder.t

  val keyCode : (Js.Json.t, int) Tea_json.Decoder.t

  val preventDefaultOn :
       ?key:(string[@ns.namedArgLoc])
    -> string
    -> ('event, 'msg) Tea_json.Decoder.t
    -> 'msg Vdom.property

  val onClick : 'msg -> 'msg Vdom.property

  val onDoubleClick : 'msg -> 'msg Vdom.property

  val onMouseDown : 'msg -> 'msg Vdom.property

  val onMouseUp : 'msg -> 'msg Vdom.property

  val onMouseEnter : 'msg -> 'msg Vdom.property

  val onMouseLeave : 'msg -> 'msg Vdom.property

  val onMouseOver : 'msg -> 'msg Vdom.property

  val onMouseOut : 'msg -> 'msg Vdom.property

  val onInputOpt :
       ?key:(string[@ns.namedArgLoc])
    -> (string -> 'msg option)
    -> 'msg Vdom.property

  val onInput :
    ?key:(string[@ns.namedArgLoc]) -> (string -> 'msg) -> 'msg Vdom.property

  val onCheckOpt :
       ?key:(string[@ns.namedArgLoc])
    -> (bool -> 'msg option)
    -> 'msg Vdom.property

  val onCheck :
    ?key:(string[@ns.namedArgLoc]) -> (bool -> 'msg) -> 'msg Vdom.property

  val onChangeOpt :
       ?key:(string[@ns.namedArgLoc])
    -> (string -> 'msg option)
    -> 'msg Vdom.property

  val onChange :
    ?key:(string[@ns.namedArgLoc]) -> (string -> 'msg) -> 'msg Vdom.property

  val onSubmit : 'msg -> 'msg Vdom.property

  val onBlur : 'msg -> 'msg Vdom.property

  val onFocus : 'msg -> 'msg Vdom.property
end
