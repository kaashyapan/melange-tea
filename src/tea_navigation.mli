module Location : sig
  type nonrec t =
    { href: string
    ; protocol: string
    ; host: string
    ; hostname: string
    ; port: string
    ; pathname: string
    ; search: string
    ; hash: string
    ; username: string
    ; password: string
    ; origin: string }

  val get : unit -> t
end

type nonrec ('flags, 'model, 'msg) navigationProgram =
  { init: 'flags -> Location.t -> 'model * 'msg Tea_cmd.t
  ; update: 'model -> 'msg -> 'model * 'msg Tea_cmd.t
  ; view: 'model -> 'msg Vdom.t
  ; subscriptions: 'model -> 'msg Tea_sub.t
  ; shutdown: 'model -> 'msg Tea_cmd.t }

val notifier : (Location.t -> unit) option ref

val notifyUrlChange : unit -> unit

val subscribe : (Location.t -> 'a) -> 'a Tea_sub.t

val replaceState : string -> unit

val pushState : string -> unit

val modifyUrl : string -> 'a Tea_cmd.t

val newUrl : string -> 'a Tea_cmd.t

val go : int -> 'a Tea_cmd.t

val back : int -> 'a Tea_cmd.t

val forward : int -> 'a Tea_cmd.t

val navigationProgram :
     (Location.t -> 'a)
  -> ('b, 'c, 'a) navigationProgram
  -> Dom.node Js.null_undefined
  -> 'b
  -> 'a Tea_app.programInterface
