val accentHeight : string -> 'msg Vdom.property

val accelerate : string -> 'msg Vdom.property

val accumulate : string -> 'msg Vdom.property

val additive : string -> 'msg Vdom.property

val alphabetic : string -> 'msg Vdom.property

val allowReorder : string -> 'msg Vdom.property

val amplitude : string -> 'msg Vdom.property

val arabicForm : string -> 'msg Vdom.property

val ascent : string -> 'msg Vdom.property

val attributeName : string -> 'msg Vdom.property

val attributeType : string -> 'msg Vdom.property

val autoReverse : string -> 'msg Vdom.property

val azimuth : string -> 'msg Vdom.property

val baseFrequency : string -> 'msg Vdom.property

val baseProfile : string -> 'msg Vdom.property

val bbox : string -> 'msg Vdom.property

val begin' : string -> 'msg Vdom.property

val bias : string -> 'msg Vdom.property

val by : string -> 'msg Vdom.property

val calcMode : string -> 'msg Vdom.property

val capHeight : string -> 'msg Vdom.property

val class' : string -> 'msg Vdom.property

val clipPathUnits : string -> 'msg Vdom.property

val contentScriptType : string -> 'msg Vdom.property

val contentStyleType : string -> 'msg Vdom.property

val cx : string -> 'msg Vdom.property

val cy : string -> 'msg Vdom.property

val d : string -> 'msg Vdom.property

val decelerate : string -> 'msg Vdom.property

val descent : string -> 'msg Vdom.property

val diffuseConstant : string -> 'msg Vdom.property

val divisor : string -> 'msg Vdom.property

val dur : string -> 'msg Vdom.property

val dx : string -> 'msg Vdom.property

val dy : string -> 'msg Vdom.property

val edgeMode : string -> 'msg Vdom.property

val elevation : string -> 'msg Vdom.property

val end' : string -> 'msg Vdom.property

val exponent : string -> 'msg Vdom.property

val externalResourcesRequired : string -> 'msg Vdom.property

val filterRes : string -> 'msg Vdom.property

val filterUnits : string -> 'msg Vdom.property

val format : string -> 'msg Vdom.property

val from : string -> 'msg Vdom.property

val fx : string -> 'msg Vdom.property

val fy : string -> 'msg Vdom.property

val g1 : string -> 'msg Vdom.property

val g2 : string -> 'msg Vdom.property

val glyphName : string -> 'msg Vdom.property

val glyphRef : string -> 'msg Vdom.property

val gradientTransform : string -> 'msg Vdom.property

val gradientUnits : string -> 'msg Vdom.property

val hanging : string -> 'msg Vdom.property

val height : string -> 'msg Vdom.property

val horizAdvX : string -> 'msg Vdom.property

val horizOriginX : string -> 'msg Vdom.property

val horizOriginY : string -> 'msg Vdom.property

val id : string -> 'msg Vdom.property

val ideographic : string -> 'msg Vdom.property

val in' : string -> 'msg Vdom.property

val in2 : string -> 'msg Vdom.property

val intercept : string -> 'msg Vdom.property

val k : string -> 'msg Vdom.property

val k1 : string -> 'msg Vdom.property

val k2 : string -> 'msg Vdom.property

val k3 : string -> 'msg Vdom.property

val k4 : string -> 'msg Vdom.property

val kernelMatrix : string -> 'msg Vdom.property

val kernelUnitLength : string -> 'msg Vdom.property

val keyPoints : string -> 'msg Vdom.property

val keySplines : string -> 'msg Vdom.property

val keyTimes : string -> 'msg Vdom.property

val lang : string -> 'msg Vdom.property

val lengthAdjust : string -> 'msg Vdom.property

val limitingConeAngle : string -> 'msg Vdom.property

val local : string -> 'msg Vdom.property

val markerHeight : string -> 'msg Vdom.property

val markerUnits : string -> 'msg Vdom.property

val markerWidth : string -> 'msg Vdom.property

val maskContentUnits : string -> 'msg Vdom.property

val maskUnits : string -> 'msg Vdom.property

val mathematical : string -> 'msg Vdom.property

val max : string -> 'msg Vdom.property

val media : string -> 'msg Vdom.property

val method' : string -> 'msg Vdom.property

val min : string -> 'msg Vdom.property

val mode : string -> 'msg Vdom.property

val name : string -> 'msg Vdom.property

val numOctaves : string -> 'msg Vdom.property

val offset : string -> 'msg Vdom.property

val operator : string -> 'msg Vdom.property

val order : string -> 'msg Vdom.property

val orient : string -> 'msg Vdom.property

val orientation : string -> 'msg Vdom.property

val origin : string -> 'msg Vdom.property

val overlinePosition : string -> 'msg Vdom.property

val overlineThickness : string -> 'msg Vdom.property

val panose1 : string -> 'msg Vdom.property

val path : string -> 'msg Vdom.property

val pathLength : string -> 'msg Vdom.property

val patternContentUnits : string -> 'msg Vdom.property

val patternTransform : string -> 'msg Vdom.property

val patternUnits : string -> 'msg Vdom.property

val pointOrder : string -> 'msg Vdom.property

val points : string -> 'msg Vdom.property

val pointsAtX : string -> 'msg Vdom.property

val pointsAtY : string -> 'msg Vdom.property

val pointsAtZ : string -> 'msg Vdom.property

val preserveAlpha : string -> 'msg Vdom.property

val preserveAspectRatio : string -> 'msg Vdom.property

val primitiveUnits : string -> 'msg Vdom.property

val r : string -> 'msg Vdom.property

val radius : string -> 'msg Vdom.property

val refX : string -> 'msg Vdom.property

val refY : string -> 'msg Vdom.property

val renderingIntent : string -> 'msg Vdom.property

val repeatCount : string -> 'msg Vdom.property

val repeatDur : string -> 'msg Vdom.property

val requiredExtensions : string -> 'msg Vdom.property

val requiredFeatures : string -> 'msg Vdom.property

val restart : string -> 'msg Vdom.property

val result : string -> 'msg Vdom.property

val rotate : string -> 'msg Vdom.property

val rx : string -> 'msg Vdom.property

val ry : string -> 'msg Vdom.property

val scale : string -> 'msg Vdom.property

val seed : string -> 'msg Vdom.property

val slope : string -> 'msg Vdom.property

val spacing : string -> 'msg Vdom.property

val specularConstant : string -> 'msg Vdom.property

val specularExponent : string -> 'msg Vdom.property

val speed : string -> 'msg Vdom.property

val spreadMethod : string -> 'msg Vdom.property

val startOffset : string -> 'msg Vdom.property

val stdDeviation : string -> 'msg Vdom.property

val stemh : string -> 'msg Vdom.property

val stemv : string -> 'msg Vdom.property

val stitchTiles : string -> 'msg Vdom.property

val strikethroughPosition : string -> 'msg Vdom.property

val strikethroughThickness : string -> 'msg Vdom.property

val string : string -> 'msg Vdom.property

val style : string -> 'msg Vdom.property

val surfaceScale : string -> 'msg Vdom.property

val systemLanguage : string -> 'msg Vdom.property

val tableValues : string -> 'msg Vdom.property

val target : string -> 'msg Vdom.property

val targetX : string -> 'msg Vdom.property

val targetY : string -> 'msg Vdom.property

val textLength : string -> 'msg Vdom.property

val title : string -> 'msg Vdom.property

val to' : string -> 'msg Vdom.property

val transform : string -> 'msg Vdom.property

val type' : string -> 'msg Vdom.property

val u1 : string -> 'msg Vdom.property

val u2 : string -> 'msg Vdom.property

val underlinePosition : string -> 'msg Vdom.property

val underlineThickness : string -> 'msg Vdom.property

val unicode : string -> 'msg Vdom.property

val unicodeRange : string -> 'msg Vdom.property

val unitsPerEm : string -> 'msg Vdom.property

val vAlphabetic : string -> 'msg Vdom.property

val vHanging : string -> 'msg Vdom.property

val vIdeographic : string -> 'msg Vdom.property

val vMathematical : string -> 'msg Vdom.property

val values : string -> 'msg Vdom.property

val version : string -> 'msg Vdom.property

val vertAdvY : string -> 'msg Vdom.property

val vertOriginX : string -> 'msg Vdom.property

val vertOriginY : string -> 'msg Vdom.property

val viewBox : string -> 'msg Vdom.property

val viewTarget : string -> 'msg Vdom.property

val width : string -> 'msg Vdom.property

val widths : string -> 'msg Vdom.property

val x : string -> 'msg Vdom.property

val xHeight : string -> 'msg Vdom.property

val x1 : string -> 'msg Vdom.property

val x2 : string -> 'msg Vdom.property

val xChannelSelector : string -> 'msg Vdom.property

val xlinkActuate : string -> 'msg Vdom.property

val xlinkArcrole : string -> 'msg Vdom.property

val xlinkHref : string -> 'msg Vdom.property

val xlinkRole : string -> 'msg Vdom.property

val xlinkShow : string -> 'msg Vdom.property

val xlinkTitle : string -> 'msg Vdom.property

val xlinkType : string -> 'msg Vdom.property

val xmlBase : string -> 'msg Vdom.property

val xmlLang : string -> 'msg Vdom.property

val xmlSpace : string -> 'msg Vdom.property

val y : string -> 'msg Vdom.property

val y1 : string -> 'msg Vdom.property

val y2 : string -> 'msg Vdom.property

val yChannelSelector : string -> 'msg Vdom.property

val z : string -> 'msg Vdom.property

val zoomAndPan : string -> 'msg Vdom.property

val alignmentBaseline : string -> 'msg Vdom.property

val baselineShift : string -> 'msg Vdom.property

val clipPath : string -> 'msg Vdom.property

val clipRule : string -> 'msg Vdom.property

val clip : string -> 'msg Vdom.property

val colorInterpolationFilters : string -> 'msg Vdom.property

val colorInterpolation : string -> 'msg Vdom.property

val colorProfile : string -> 'msg Vdom.property

val colorRendering : string -> 'msg Vdom.property

val color : string -> 'msg Vdom.property

val cursor : string -> 'msg Vdom.property

val direction : string -> 'msg Vdom.property

val display : string -> 'msg Vdom.property

val dominantBaseline : string -> 'msg Vdom.property

val enableBackground : string -> 'msg Vdom.property

val fillOpacity : string -> 'msg Vdom.property

val fillRule : string -> 'msg Vdom.property

val fill : string -> 'msg Vdom.property

val filter : string -> 'msg Vdom.property

val floodColor : string -> 'msg Vdom.property

val floodOpacity : string -> 'msg Vdom.property

val fontFamily : string -> 'msg Vdom.property

val fontSizeAdjust : string -> 'msg Vdom.property

val fontSize : string -> 'msg Vdom.property

val fontStretch : string -> 'msg Vdom.property

val fontStyle : string -> 'msg Vdom.property

val fontVariant : string -> 'msg Vdom.property

val fontWeight : string -> 'msg Vdom.property

val glyphOrientationHorizontal : string -> 'msg Vdom.property

val glyphOrientationVertical : string -> 'msg Vdom.property

val imageRendering : string -> 'msg Vdom.property

val kerning : string -> 'msg Vdom.property

val letterSpacing : string -> 'msg Vdom.property

val lightingColor : string -> 'msg Vdom.property

val markerEnd : string -> 'msg Vdom.property

val markerMid : string -> 'msg Vdom.property

val markerStart : string -> 'msg Vdom.property

val mask : string -> 'msg Vdom.property

val opacity : string -> 'msg Vdom.property

val overflow : string -> 'msg Vdom.property

val pointerEvents : string -> 'msg Vdom.property

val shapeRendering : string -> 'msg Vdom.property

val stopColor : string -> 'msg Vdom.property

val stopOpacity : string -> 'msg Vdom.property

val strokeDasharray : string -> 'msg Vdom.property

val strokeDashoffset : string -> 'msg Vdom.property

val strokeLinecap : string -> 'msg Vdom.property

val strokeLinejoin : string -> 'msg Vdom.property

val strokeMiterlimit : string -> 'msg Vdom.property

val strokeOpacity : string -> 'msg Vdom.property

val strokeWidth : string -> 'msg Vdom.property

val stroke : string -> 'msg Vdom.property

val textAnchor : string -> 'msg Vdom.property

val textDecoration : string -> 'msg Vdom.property

val textRendering : string -> 'msg Vdom.property

val unicodeBidi : string -> 'msg Vdom.property

val visibility : string -> 'msg Vdom.property

val wordSpacing : string -> 'msg Vdom.property

val writingMode : string -> 'msg Vdom.property
