type nonrec t = float

let every ~key:(key [@ns.namedArgLoc]) (interval : float) tagger =
  ((let open Vdom in
    let enableCall callbacks =
      (let id =
         Js.Global.setIntervalFloat
           (fun () -> callbacks.enqueue (tagger (Js.Date.now ())))
           interval
       in
       fun () -> Js.Global.clearInterval id )
      [@ns.braces]
    in
    Tea_sub.registration key enableCall ) [@ns.braces] )

let delay (msTime : float) msg =
  Tea_cmd.call (fun callbacks ->
      (let _unhandledID =
         Js.Global.setTimeoutFloat
           (fun () -> (callbacks.contents.enqueue msg [@ns.braces]))
           msTime
       in
       () )
      [@ns.braces] )

let millisecond = 1.0

let second = 1000.0 *. millisecond

let minute = 60.0 *. second

let hour = 60.0 *. minute

let inMilliseconds t = t

let inSeconds t = t /. second

let inMinutes t = t /. minute

let inHours t = t /. hour
