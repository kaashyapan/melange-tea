let renderEvent ?key:((key [@ns.namedArgLoc]) = "") msg =
  ((let open Vdom in
    let enableCall callbacks =
      (let () = callbacks.on (AddRenderMsg msg) in
       fun () -> callbacks.on (RemoveRenderMsg msg) )
      [@ns.braces]
    in
    Tea_sub.registration key enableCall ) [@ns.braces] )

module LocalStorage = struct
  let nativeBinding = Tea_task.nativeBinding

  let (length : (int, string) Tea_task.t) =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage2.length Dom.Storage.localStorage)) )

  let (clear : (unit, string) Tea_task.t) =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage2.clear Dom.Storage.localStorage)) )

  let clearCmd () = Tea_task.attemptOpt (fun _ -> None) clear

  let key idx : (string option, string) Tea_task.t =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage2.key Dom.Storage.localStorage idx)) )

  let getItem key : (string option, string) Tea_task.t =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage2.getItem Dom.Storage.localStorage key)) )

  let removeItem key : (unit, string) Tea_task.t =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage2.removeItem Dom.Storage.localStorage key)) )

  let removeItemCmd key = Tea_task.attemptOpt (fun _ -> None) (removeItem key)

  let setItem key value : (unit, string) Tea_task.t =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage2.setItem Dom.Storage.localStorage key value)) )

  let setItemCmd key value =
    Tea_task.attemptOpt (fun _ -> None) (setItem key value)
end
