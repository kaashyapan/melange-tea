val minInt : int

val maxInt : int

val minFloat : float

val maxFloat : float

type nonrec 'typ t

val bool : bool t

val int : int -> int -> int t

val float : float -> float -> float t

val list : int -> 'msg t -> 'msg list t

val map : ('msg -> 'b) -> 'msg t -> 'b t

val map2 : ('msg -> 'b -> 'c) -> 'msg t -> 'b t -> 'c t

val map3 : ('msg -> 'b -> 'c -> 'd) -> 'msg t -> 'b t -> 'c t -> 'd t

val map4 :
  ('msg -> 'b -> 'c -> 'd -> 'e) -> 'msg t -> 'b t -> 'c t -> 'd t -> 'e t

val map5 :
     ('msg -> 'b -> 'c -> 'd -> 'e -> 'f)
  -> 'msg t
  -> 'b t
  -> 'c t
  -> 'd t
  -> 'e t
  -> 'f t

val andThen : ('msg -> 'b t) -> 'msg t -> 'b t

val pair : 'msg t -> 'b t -> ('msg * 'b) t

val generate : ('msg -> 'b) -> 'msg t -> 'b Tea_cmd.t

type nonrec seed = Seed of Random.State.t

val step : 'msg t -> seed -> 'msg * seed

val initialSeed : int -> seed
