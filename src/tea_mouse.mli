type nonrec position = {x: int; y: int}

val position : (Js.Json.t, position) Tea_json.Decoder.t

val registerGlobal : string -> string -> (position -> 'a) -> 'a Tea_sub.t

val clicks : ?key:(string[@ns.namedArgLoc]) -> (position -> 'a) -> 'a Tea_sub.t

val moves : ?key:(string[@ns.namedArgLoc]) -> (position -> 'a) -> 'a Tea_sub.t

val downs : ?key:(string[@ns.namedArgLoc]) -> (position -> 'a) -> 'a Tea_sub.t

val ups : ?key:(string[@ns.namedArgLoc]) -> (position -> 'a) -> 'a Tea_sub.t
