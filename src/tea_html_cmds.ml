let focus id =
  Tea_cmd.call (fun _enqueue ->
      (let ecb _ =
         (let element =
            Webapi.Dom.Document.getElementById id Webapi.Dom.document
            |. Belt.Option.flatMap Webapi.Dom.HtmlElement.ofElement
          in
          match element with
          | None ->
              Js.log ("Attempted to focus a non-existant element of: ", id)
          | Some elem ->
              Webapi.Dom.HtmlElement.focus elem )
         [@ns.braces]
       in
       let cb _ = ignore (Webapi.requestCancellableAnimationFrame ecb) in
       ignore (Webapi.requestCancellableAnimationFrame cb) ;
       () )
      [@ns.braces] )
