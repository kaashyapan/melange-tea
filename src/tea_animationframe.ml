type nonrec t = {time: Tea_time.t; delta: Tea_time.t}

let every ?key:((key [@ns.namedArgLoc]) = "") tagger =
  ((let open Vdom in
    let enableCall callbacks =
      (let lastTime = ref (Js.Date.now ()) in
       let id = ref None in
       let rec onFrame _time =
         (let time = Js.Date.now () in
          match id.contents with
          | None ->
              ()
          | Some _i -> (
              let ret =
                { time
                ; delta=
                    ( if time < lastTime.contents then 0.0
                      else time -. lastTime.contents ) }
              in
              let () = lastTime := time in
              let () = callbacks.enqueue (tagger ret) in
              match id.contents with
              | None ->
                  ()
              | Some _stillActive ->
                  let () =
                    id := Some (Webapi.requestCancellableAnimationFrame onFrame)
                  in
                  () ) )
         [@ns.braces]
       in
       let () = id := Some (Webapi.requestCancellableAnimationFrame onFrame) in
       fun () ->
         match id.contents with
         | None ->
             ()
         | Some i ->
             let () = Webapi.cancelAnimationFrame i in
             let () = id := None in
             () )
      [@ns.braces]
    in
    Tea_sub.registration key enableCall ) [@ns.braces] )

let times ?key:((key [@ns.namedArgLoc]) = "") tagger =
  every (fun ev -> tagger ~key:(key [@ns.namedArgLoc]) ev.time)

let diffs ?key:((key [@ns.namedArgLoc]) = "") tagger =
  every (fun ev -> tagger ~key:(key [@ns.namedArgLoc]) ev.delta)
