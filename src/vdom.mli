type nonrec eventCallback = Dom.event -> unit

type nonrec 'msg systemMessage =
  | Render
  | AddRenderMsg of 'msg
  | RemoveRenderMsg of 'msg

type nonrec 'msg applicationCallbacks =
  {enqueue: 'msg -> unit; on: 'msg systemMessage -> unit}

type nonrec 'msg eventHandler =
  | EventHandlerCallback of string * (Dom.event -> 'msg option)
  | EventHandlerMsg of 'msg

type nonrec 'msg eventCache =
  {handler: eventCallback; cb: (Dom.event -> 'msg option) ref}

type nonrec 'msg property =
  | NoProp
  | RawProp of string * string
  | Attribute of string * string * string
  | Data of string * string
  | Event of string * 'msg eventHandler * 'msg eventCache option ref
  | Style of (string * string) list

type nonrec 'msg properties = 'msg property list

type 'msg t =
  | CommentNode of string
  | Text of string
  | Node of string * string * string * string * 'msg properties * 'msg t list
  | LazyGen of string * (unit -> 'msg t) * 'msg t ref
  | Tagger of
      ('msg applicationCallbacks ref -> 'msg applicationCallbacks ref) * 'msg t

val noNode : 'msg t

val comment : string -> 'msg t

val text : string -> 'msg t

val fullnode :
     string
  -> string
  -> string
  -> string
  -> 'msg properties
  -> 'msg t list
  -> 'msg t

val node :
     ?namespace:(string[@ns.namedArgLoc])
  -> string
  -> ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg properties
  -> 'msg t list
  -> 'msg t

val lazyGen : string -> (unit -> 'msg t) -> 'msg t

val noProp : 'msg property

val prop : string -> string -> 'msg property

val onCB :
     key:(string[@ns.namedArgLoc])
  -> string
  -> (Dom.event -> 'msg option)
  -> 'msg property

val onMsg : string -> 'msg -> 'msg property

val attribute : string -> string -> string -> 'msg property

val data : string -> string -> 'msg property

val style : string -> string -> 'msg property

val styles : (string * string) list -> 'msg property

val renderToHtmlString : 'msg t -> string

val emptyEventHandler : eventCallback

val emptyEventCB : 'msg -> eventCallback option

val eventHandler :
     'msg applicationCallbacks ref
  -> (Dom.event -> 'msg option) ref
  -> eventCallback

val eventHandlerGetCB : 'msg eventHandler -> Dom.event -> 'msg option

val compareEventHandlerTypes : 'msg eventHandler -> 'msg eventHandler -> bool

val eventHandlerRegister :
     'msg applicationCallbacks ref
  -> Dom.eventTarget
  -> string
  -> 'msg eventHandler
  -> 'msg eventCache option

val eventHandlerUnregister :
  Dom.eventTarget -> string -> 'msg eventCache option -> 'msg eventCache option

val eventHandlerMutate :
     'msg applicationCallbacks ref
  -> Dom.eventTarget
  -> string
  -> string
  -> 'msg eventHandler
  -> 'msg eventHandler
  -> 'msg eventCache option ref
  -> 'msg eventCache option ref
  -> unit

val patchVNodesOnElemsPropertiesApplyAdd :
  'msg applicationCallbacks ref -> Dom.element -> int -> 'msg property -> unit

val patchVNodesOnElemsPropertiesApplyRemove :
  'msg applicationCallbacks ref -> Dom.element -> int -> 'msg property -> unit

val patchVNodesOnElemsPropertiesApplyRemoveAdd :
     'msg applicationCallbacks ref
  -> Dom.element
  -> int
  -> 'msg property
  -> 'msg property
  -> unit

val patchVNodesOnElemsPropertiesApplyMutate :
     'msg applicationCallbacks ref
  -> Dom.element
  -> int
  -> 'msg property
  -> 'msg property
  -> unit

val patchVNodesOnElemsPropertiesApply :
     'msg applicationCallbacks ref
  -> Dom.element
  -> int
  -> 'msg property list
  -> 'msg property list
  -> bool

val patchVNodesOnElemsProperties :
     'msg applicationCallbacks ref
  -> Dom.element
  -> 'msg property list
  -> 'msg property list
  -> bool

val genEmptyProps : int -> 'msg property list

val mapEmptyProps : 'msg property list -> 'msg property list

val patchVNodesOnElemsReplaceNode :
     'msg applicationCallbacks ref
  -> Dom.node
  -> Dom.nodeList
  -> int
  -> 'msg t
  -> unit

val patchVNodesOnElemsCreateElement :
  'msg applicationCallbacks ref -> 'msg t -> Dom.node

val patchVNodesOnElemsMutateNode :
     'msg applicationCallbacks ref
  -> Dom.node
  -> Dom.nodeList
  -> int
  -> 'msg t
  -> 'msg t
  -> unit

val patchVNodesOnElems :
     'msg applicationCallbacks ref
  -> Dom.node
  -> Dom.nodeList
  -> int
  -> 'msg t list
  -> 'msg t list
  -> unit

val patchVNodesIntoElement :
     'msg applicationCallbacks ref
  -> Dom.node
  -> 'msg t list
  -> 'msg t list
  -> 'msg t list

val patchVNodeIntoElement :
  'msg applicationCallbacks ref -> Dom.node -> 'msg t -> 'msg t -> 'msg t list

val wrapCallbacksOn : ('msg -> 'b) -> 'msg systemMessage -> 'b systemMessage

val wrapCallbacks :
  ('msg -> 'b) -> 'b applicationCallbacks ref -> 'msg applicationCallbacks ref

val map : ('msg -> 'b) -> 'msg t -> 'b t
