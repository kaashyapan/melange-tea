type 'msg t =
  | NoSub : 'msg t
  | Batch : 'msg t list -> 'msg t
  | Registration :
      string
      * ('msg Vdom.applicationCallbacks ref -> unit -> unit)
      * (unit -> unit) option ref
      -> 'msg t
  | Mapper :
      ('msg Vdom.applicationCallbacks ref -> 'msgB Vdom.applicationCallbacks ref)
      * 'msgB t
      -> 'msg t

type nonrec 'msg applicationCallbacks = 'msg Vdom.applicationCallbacks

val none : 'msg t

val batch : 'msg t list -> 'msg t

val registration :
  string -> ('msg Vdom.applicationCallbacks -> unit -> unit) -> 'msg t

val map : ('msg -> 'b) -> 'msg t -> 'b t

val mapFunc :
     ('msg Vdom.applicationCallbacks ref -> 'b Vdom.applicationCallbacks ref)
  -> 'b t
  -> 'msg t

val run :
     'msgOld Vdom.applicationCallbacks ref
  -> 'msgNew Vdom.applicationCallbacks ref
  -> 'msgOld t
  -> 'msgNew t
  -> 'msgNew t
