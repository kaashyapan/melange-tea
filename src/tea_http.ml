type nonrec responseStatus = {code: int; message: string}

type nonrec requestBody = Web.XMLHttpRequest.body

type nonrec bodyType = Web.XMLHttpRequest.responseType

type nonrec responseBody = Web.XMLHttpRequest.responseBody

type nonrec response =
  { url: string
  ; status: responseStatus
  ; headers: string Belt.Map.String.t
  ; body: responseBody }

type nonrec 'parsedata error =
  | BadUrl of string
  | Timeout
  | NetworkError
  | Aborted
  | BadStatus of response
  | BadPayload of 'parsedata * response

let stringOfError x =
  match x with
  | BadUrl url ->
      "Bad Url: " ^ url
  | Timeout ->
      "Timeout"
  | NetworkError ->
      "Unknown network error"
  | Aborted ->
      "Request aborted"
  | BadStatus resp ->
      "Bad Status: " ^ resp.url
  | BadPayload (_customData, resp) ->
      "Bad Payload: " ^ resp.url

type nonrec header = Header of string * string

type nonrec 'res expect =
  | Expect of bodyType * (response -> ('res, string) result)

type nonrec 'msg requestEvents =
  { onreadystatechange:
      (   'msg Vdom.applicationCallbacks ref
       -> Web.XMLHttpRequest.eventReadystatechange
       -> unit )
      option
  ; onprogress:
      (   'msg Vdom.applicationCallbacks ref
       -> Web.XMLHttpRequest.eventProgress
       -> unit )
      option }

let emptyRequestEvents = {onreadystatechange= None; onprogress= None}

type nonrec 'res rawRequest =
  { method': string
  ; headers: header list
  ; url: string
  ; body: requestBody
  ; expect: 'res expect
  ; timeout: Tea_time.t option
  ; withCredentials: bool }

type nonrec ('msg, 'res) request =
  | Request of 'res rawRequest * 'msg requestEvents option

let expectStringResponse func =
  ((let open Web.XMLHttpRequest in
    Expect
      ( TextResponseType
      , fun {body; _} ->
          match body with
          | TextResponse s ->
              func s
          | _ ->
              Error "Non-text response returned" ) ) [@ns.braces] )

let expectString = expectStringResponse (fun resString -> Ok resString)

let request rawRequest = Request (rawRequest, None)

let getString url =
  request
    { method'= "GET"
    ; headers= []
    ; url
    ; body= Web.XMLHttpRequest.EmptyBody
    ; expect= expectString
    ; timeout= None
    ; withCredentials= false }

let toTask (Request (request, _maybeEvents)) =
  (let module StringMap = Belt.Map.String in
  match request with
  | {method'; headers; url; body; expect; timeout; withCredentials} ->
      let (Expect (typ, responseToResult)) = expect in
      Tea_task.nativeBinding (fun cb ->
          (let enqRes result _ev = cb result in
           let enqResError result = enqRes (Error result) in
           let enqResOk result = enqRes (Ok result) in
           let xhr = Web.XMLHttpRequest.create () in
           let setEvent ev cb = ev cb xhr in
           let () =
             setEvent Web.XMLHttpRequest.set_onerror (enqResError NetworkError)
           in
           let () =
             setEvent Web.XMLHttpRequest.set_ontimeout (enqResError Timeout)
           in
           let () =
             setEvent Web.XMLHttpRequest.set_onabort (enqResError Aborted)
           in
           let () =
             setEvent Web.XMLHttpRequest.set_onload (fun _ev ->
                 ((let open Web.XMLHttpRequest in
                   let headers =
                     match getAllResponseHeadersAsDict xhr with
                     | Error _e ->
                         StringMap.empty
                     | Ok headers ->
                         headers
                   in
                   let response =
                     { status=
                         {code= get_status xhr; message= get_statusText xhr}
                     ; headers
                     ; url= get_responseURL xhr
                     ; body= get_response xhr }
                   in
                   if response.status.code < 200 || 300 <= response.status.code
                   then enqResError (BadStatus response) ()
                   else
                     match responseToResult response with
                     | Error error ->
                         enqResError (BadPayload (error, response)) ()
                     | Ok result ->
                         enqResOk result () ) [@ns.braces] ) )
           in
           let () =
             try Web.XMLHttpRequest.open' method' url xhr
             with _ -> enqResError (BadUrl url) ()
           in
           let () =
             (let setHeader (Header (k, v)) =
                Web.XMLHttpRequest.setRequestHeader k v xhr
              in
              let () = List.iter setHeader headers in
              let () = Web.XMLHttpRequest.set_responseType typ xhr in
              let () =
                match timeout with
                | None ->
                    ()
                | Some t ->
                    Web.XMLHttpRequest.set_timeout t xhr
              in
              let () =
                Web.XMLHttpRequest.set_withCredentials withCredentials xhr
              in
              () )
             [@ns.braces]
           in
           let () = Web.XMLHttpRequest.send body xhr in
           () )
          [@ns.braces] ) )
  [@ns.braces]

let send resultToMessage (Request (request, maybeEvents)) =
  (let module StringMap = Belt.Map.String in
  match request with
  | {method'; headers; url; body; expect; timeout; withCredentials} ->
      let (Expect (typ, responseToResult)) = expect in
      Tea_cmd.call (fun callbacks ->
          (let enqRes result _ev =
             ((let open Vdom in
               callbacks.contents.enqueue (resultToMessage result) )
             [@ns.braces] )
           in
           let enqResError result = enqRes (Error result) in
           let enqResOk result = enqRes (Ok result) in
           let xhr = Web.XMLHttpRequest.create () in
           let setEvent ev cb = ev cb xhr in
           let () =
             match maybeEvents with
             | None ->
                 ()
             | Some {onprogress; onreadystatechange} ->
                 let open Web.XMLHttpRequest in
                 let mayCB thenDo x =
                   match x with None -> () | Some v -> thenDo (v callbacks)
                 in
                 let () =
                   mayCB (setEvent set_onreadystatechange) onreadystatechange
                 in
                 let () = mayCB (setEvent set_onprogress) onprogress in
                 ()
           in
           let () =
             setEvent Web.XMLHttpRequest.set_onerror (enqResError NetworkError)
           in
           let () =
             setEvent Web.XMLHttpRequest.set_ontimeout (enqResError Timeout)
           in
           let () =
             setEvent Web.XMLHttpRequest.set_onabort (enqResError Aborted)
           in
           let () =
             setEvent Web.XMLHttpRequest.set_onload (fun _ev ->
                 ((let open Web.XMLHttpRequest in
                   let headers =
                     match getAllResponseHeadersAsDict xhr with
                     | Error _e ->
                         StringMap.empty
                     | Ok headers ->
                         headers
                   in
                   let response =
                     { status=
                         {code= get_status xhr; message= get_statusText xhr}
                     ; headers
                     ; url= get_responseURL xhr
                     ; body= get_response xhr }
                   in
                   if response.status.code < 200 || 300 <= response.status.code
                   then enqResError (BadStatus response) ()
                   else
                     match responseToResult response with
                     | Error error ->
                         enqResError (BadPayload (error, response)) ()
                     | Ok result ->
                         enqResOk result () ) [@ns.braces] ) )
           in
           let () =
             try Web.XMLHttpRequest.open' method' url xhr
             with _ -> enqResError (BadUrl url) ()
           in
           let () =
             (let setHeader (Header (k, v)) =
                Web.XMLHttpRequest.setRequestHeader k v xhr
              in
              let () = List.iter setHeader headers in
              let () = Web.XMLHttpRequest.set_responseType typ xhr in
              let () =
                match timeout with
                | None ->
                    ()
                | Some t ->
                    Web.XMLHttpRequest.set_timeout t xhr
              in
              let () =
                Web.XMLHttpRequest.set_withCredentials withCredentials xhr
              in
              () )
             [@ns.braces]
           in
           let () = Web.XMLHttpRequest.send body xhr in
           () )
          [@ns.braces] ) )
  [@ns.braces]

external encodeURIComponent : string -> string = "encodeURIComponent" [@@val]

let encodeUri str = encodeURIComponent str

external decodeURIComponent : string -> string = "decodeURIComponent" [@@val]

let decodeUri str = try Some (decodeURIComponent str) with _ -> None

module Progress = struct
  type nonrec t = {bytes: int; bytesExpected: int}

  let emptyProgress = {bytes= 0; bytesExpected= 0}

  let track toMessage (Request (request, events)) =
    (let onprogress =
       Some
         (fun callbacks ev ->
           ((let open Vdom in
             let lengthComputable =
               ((let open Tea_json.Decoder in
                 match decodeValue (field "lengthComputable" bool) ev with
                 | Error _e ->
                     false
                 | Ok v ->
                     v ) [@ns.braces] )
             in
             if lengthComputable then
               let open Tea_json.Decoder in
               let decoder =
                 map2
                   (fun bytes bytesExpected -> {bytes; bytesExpected})
                   (field "loaded" int) (field "total" int)
               in
               match decodeValue decoder ev with
               | Error _e ->
                   ()
               | Ok t ->
                   callbacks.contents.enqueue (toMessage t) ) [@ns.braces] ) )
     in
     let events =
       match events with None -> emptyRequestEvents | Some e -> e
     in
     Request (request, Some {events with onprogress}) )
    [@ns.braces]
end
