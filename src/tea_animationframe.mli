type nonrec t = {time: Tea_time.t; delta: Tea_time.t}

val every : ?key:(string[@ns.namedArgLoc]) -> (t -> 'msg) -> 'msg Tea_sub.t

val times :
     ?key:(string[@ns.namedArgLoc])
  -> (key:(string[@ns.namedArgLoc]) -> Tea_time.t -> 'msg)
  -> 'msg Tea_sub.t

val diffs :
     ?key:(string[@ns.namedArgLoc])
  -> (key:(string[@ns.namedArgLoc]) -> Tea_time.t -> 'msg)
  -> 'msg Tea_sub.t
