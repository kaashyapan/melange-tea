val cmd : 'msg Js.Promise.t -> ('msg -> 'b option) -> 'b Tea_cmd.t

val result : 'msg Js.Promise.t -> (('msg, string) result -> 'b) -> 'b Tea_cmd.t
