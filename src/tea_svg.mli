val svgNamespace : string

val noNode : 'msg Vdom.t

val text : string -> 'msg Vdom.t

val lazy1 : string -> (unit -> 'msg Vdom.t) -> 'msg Vdom.t

val node :
     string
  -> ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val svg :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val foreignObject :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val animate :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val animateColor :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val animateMotion :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val animateTransform :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val mpath :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val set :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val a :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val defs :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val g :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val marker :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val mask :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val missingGlyph :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val pattern :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val switch :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val symbol :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val desc :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val metadata :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val title :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feBlend :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feColorMatrix :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feComponentTransfer :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feComposite :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feConvolveMatrix :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feDiffuseLighting :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feDisplacementMap :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feFlood :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feFuncA :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feFuncB :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feFuncG :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feFuncR :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feGaussianBlur :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feImage :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feMerge :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feMergeNode :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feMorphology :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feOffset :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feSpecularLighting :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feTile :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feTurbulence :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val font :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fontFace :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fontFaceFormat :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fontFaceName :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fontFaceSrc :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fontFaceUri :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val hkern :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val vkern :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val linearGradient :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val radialGradient :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val stop :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val circle :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val ellipse :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val svgimage :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val line :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val path :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val polygon :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val polyline :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val rect :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val use :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feDistantLight :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fePointLight :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feSpotLight :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val altGlyph :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val altGlyphDef :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val altGlyphItem :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val glyph :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val glyphRef :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val textPath :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val text' :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val tref :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val tspan :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val clipPath :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val svgcolorProfile :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val cursor :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val filter :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val script :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val style :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val view :
     ?key:(string[@ns.namedArgLoc])
  -> ?unique:(string[@ns.namedArgLoc])
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t
