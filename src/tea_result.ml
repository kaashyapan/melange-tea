let resultToOption x = match x with Ok a -> Some a | Error _ -> None

let first fst x = match x with Error _ as e -> e | Ok _ -> fst

let errorOfFirst fst x =
  match x with
  | Error e ->
      Some e
  | Ok _ -> (
    match fst with Ok _ -> None | Error e -> Some e )
