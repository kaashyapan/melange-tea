module Decoder : sig
  type nonrec error = String.t

  module ObjectDict : sig
    type key = String.t

    type 'msg t = 'msg Belt.Map.String.t
  end

  type nonrec ('input, 'result) t =
    | Decoder of ('input -> ('result, error) result)

  type exn += ParseFail of string

  val string : (Js.Json.t, string) t

  val int : (Js.Json.t, int) t

  val float : (Js.Json.t, float) t

  val bool : (Js.Json.t, bool) t

  val null : 'msg -> (Js.Json.t, 'msg) t

  val list : (Js.Json.t, 'msg) t -> (Js.Json.t, 'msg list) t

  val array : (Js.Json.t, 'msg) t -> (Js.Json.t, 'msg array) t

  val keyValuePairs :
    (Js.Json.t, 'msg) t -> (Js.Json.t, (Js.Dict.key * 'msg) list) t

  val dict : (Js.Json.t, 'msg) t -> (Js.Json.t, 'msg ObjectDict.t) t

  val field : Js.Dict.key -> (Js.Json.t, 'msg) t -> (Js.Json.t, 'msg) t

  val at : Js.Dict.key list -> (Js.Json.t, 'msg) t -> (Js.Json.t, 'msg) t

  val index : int -> (Js.Json.t, 'msg) t -> (Js.Json.t, 'msg) t

  val maybe : ('msg, 'b) t -> ('msg, 'b option) t

  val oneOf : ('msg, 'b) t list -> ('msg, 'b) t

  val map : ('msg -> 'b) -> ('c, 'msg) t -> ('c, 'b) t

  val map2 : ('msg -> 'b -> 'c) -> ('d, 'msg) t -> ('d, 'b) t -> ('d, 'c) t

  val map3 :
       ('msg -> 'b -> 'c -> 'd)
    -> ('e, 'msg) t
    -> ('e, 'b) t
    -> ('e, 'c) t
    -> ('e, 'd) t

  val map4 :
       ('msg -> 'b -> 'c -> 'd -> 'e)
    -> ('f, 'msg) t
    -> ('f, 'b) t
    -> ('f, 'c) t
    -> ('f, 'd) t
    -> ('f, 'e) t

  val map5 :
       ('msg -> 'b -> 'c -> 'd -> 'e -> 'f)
    -> ('g, 'msg) t
    -> ('g, 'b) t
    -> ('g, 'c) t
    -> ('g, 'd) t
    -> ('g, 'e) t
    -> ('g, 'f) t

  val map6 :
       ('msg -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g)
    -> ('h, 'msg) t
    -> ('h, 'b) t
    -> ('h, 'c) t
    -> ('h, 'd) t
    -> ('h, 'e) t
    -> ('h, 'f) t
    -> ('h, 'g) t

  val map7 :
       ('msg -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h)
    -> ('i, 'msg) t
    -> ('i, 'b) t
    -> ('i, 'c) t
    -> ('i, 'd) t
    -> ('i, 'e) t
    -> ('i, 'f) t
    -> ('i, 'g) t
    -> ('i, 'h) t

  val map8 :
       ('msg -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i)
    -> ('j, 'msg) t
    -> ('j, 'b) t
    -> ('j, 'c) t
    -> ('j, 'd) t
    -> ('j, 'e) t
    -> ('j, 'f) t
    -> ('j, 'g) t
    -> ('j, 'h) t
    -> ('j, 'i) t

  val succeed : 'msg -> ('b, 'msg) t

  val fail : error -> ('msg, 'b) t

  val value : ('msg, 'msg) t

  val andThen : ('msg -> ('b, 'c) t) -> ('b, 'msg) t -> ('b, 'c) t

  val lazy_ : (unit -> ('msg, 'b) t) -> ('msg, 'b) t

  val nullable : (Js.Json.t, 'msg) t -> (Js.Json.t, 'msg option) t

  val decodeValue : ('msg, 'b) t -> 'msg -> ('b, error) result

  val decodeEvent : ('msg, 'b) t -> Dom.event -> ('b, error) result

  val decodeString : (Js.Json.t, 'msg) t -> string -> ('msg, error) result
end

module Encoder : sig
  type nonrec t = Js.Json.t

  val encode : int -> 'msg -> string

  val string : string -> Js.Json.t

  val int : int -> Js.Json.t

  val float : float -> Js.Json.t

  val bool : bool -> Js.Json.t

  val null : Js.Json.t

  val object_ : (Js.Dict.key * Js.Json.t) list -> Js.Json.t

  val array : Js.Json.t array -> Js.Json.t

  val list : Js.Json.t list -> Js.Json.t
end

type nonrec t = Js.Json.t
