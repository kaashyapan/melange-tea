type nonrec eventCallback = Dom.event -> unit
type nonrec 'msg systemMessage =
  | Render 
  | AddRenderMsg of 'msg 
  | RemoveRenderMsg of 'msg 
type nonrec 'msg applicationCallbacks =
  {
  enqueue: 'msg -> unit ;
  on: 'msg systemMessage -> unit }
type nonrec 'msg eventHandler =
  | EventHandlerCallback of string * (Dom.event -> 'msg option) 
  | EventHandlerMsg of 'msg 
type nonrec 'msg eventCache =
  {
  handler: eventCallback ;
  cb: (Dom.event -> 'msg option) ref }
type nonrec 'msg property =
  | NoProp 
  | RawProp of string * string 
  | Attribute of string * string * string 
  | Data of string * string 
  | Event of string * 'msg eventHandler * 'msg eventCache option ref 
  | Style of (string * string) list 
type nonrec 'msg properties = 'msg property list
type 'msg t =
  | CommentNode of string 
  | Text of string 
  | Node of string * string * string * string * 'msg properties * 'msg t list
  
  | LazyGen of string * (unit -> 'msg t) * 'msg t ref 
  | Tagger of
  ('msg applicationCallbacks ref -> 'msg applicationCallbacks ref) * 'msg t 
let (noNode : 'msg t) = (CommentNode "" : 'msg t)
let comment (s : string) = (CommentNode s : 'msg t)
let text (s : string) = (Text s : 'msg t)
let fullnode (namespace : string) (tagName : string) (key : string)
  (unique : string) (props : 'msg properties) (vdoms : 'msg t list) =
  (Node (namespace, tagName, key, unique, props, vdoms) : 'msg t)
let node ?namespace:((((namespace : string))[@ns.namedArgLoc ])= "") 
  (tagName : string) ?key:((((key : string))[@ns.namedArgLoc ])= "") 
  ?unique:((((unique : string))[@ns.namedArgLoc ])= "") 
  (props : 'msg properties) (vdoms : 'msg t list) =
  (fullnode namespace tagName key unique props vdoms : 'msg t)
let lazyGen (key : string) (fn : unit -> 'msg t) =
  (LazyGen (key, fn, (ref noNode)) : 'msg t)
let (noProp : 'msg property) = (NoProp : 'msg property)
let prop (propName : string) (value : string) =
  (RawProp (propName, value) : 'msg property)
let onCB ~key:(((key : string))[@ns.namedArgLoc ])  (eventName : string)
  (cb : Dom.event -> 'msg option) =
  (Event (eventName, (EventHandlerCallback (key, cb)), (ref None)) : 
  'msg property)
let onMsg (name : string) (msg : 'msg) =
  (Event (name, (EventHandlerMsg msg), (ref None)) : 'msg property)
let attribute (namespace : string) (key : string) (value : string) =
  (Attribute (namespace, key, value) : 'msg property)
let data (key : string) (value : string) =
  (Data (key, value) : 'msg property)
let style (key : string) (value : string) =
  (Style [(key, value)] : 'msg property)
let styles s = (Style s : 'msg property)
let createElementNsOptional namespace tagName =
  ((let document = Webapi.Dom.document in
    match namespace with
    | "" -> Webapi.Dom.Document.createElement tagName document
    | ns -> Webapi.Dom.Document.createElementNS ns tagName document)
  [@ns.braces ])
let nodeAt (index : int) (nodes : Dom.nodeList) =
  ((Webapi.Dom.NodeList.item index nodes) |> Belt.Option.getExn : Dom.node)
external setItem : Dom.element -> 'key -> 'value -> unit = ""[@@set_index ]
external _getItem : Dom.element -> 'key -> 'value = ""[@@get_index ]
let delItem (elem : Dom.element) (key : 'key) =
  setItem elem key Js.Undefined.empty
let rec (renderToHtmlString : 'msg t -> string) =
  (fun x ->
     match x with
     | CommentNode s -> "<!-- " ^ (s ^ " -->")
     | Text s -> s
     | Node (namespace, tagName, _key, _unique, props, vdoms) ->
         let renderProp x =
           match x with
           | NoProp -> ""
           | RawProp (k, v) -> String.concat "" [" "; k; "=\""; v; "\""]
           | Attribute (_namespace, k, v) ->
               String.concat "" [" "; k; "=\""; v; "\""]
           | Data (k, v) -> String.concat "" [" data-"; k; "=\""; v; "\""]
           | Event (_, _, _) -> ""
           | Style s ->
               String.concat ""
                 [" style=\"";
                 String.concat ";"
                   (List.map
                      (fun (k, v) -> String.concat "" [k; ":"; v; ";"]) s);
                 "\""] in
         String.concat ""
           ["<";
           namespace;
           if namespace = "" then "" else ":";
           tagName;
           String.concat "" (List.map (fun p -> renderProp p) props);
           ">";
           String.concat "" (List.map (fun v -> renderToHtmlString v) vdoms);
           "</";
           tagName;
           ">"]
     | LazyGen (_key, gen, _cache) ->
         let vdom = gen () in renderToHtmlString vdom
     | Tagger (_tagger, vdom) -> renderToHtmlString vdom : 'msg t -> string)
let (emptyEventHandler : eventCallback) = (fun _ev -> () : eventCallback)
let emptyEventCB _ev = (None : eventCallback option)
let eventHandler (callbacks : 'msg applicationCallbacks ref)
  (cb : (Dom.event -> 'msg option) ref) =
  (fun ev ->
     match cb.contents ev with
     | None -> ()
     | Some msg -> (callbacks.contents).enqueue msg : eventCallback)
let (eventHandlerGetCB : 'msg eventHandler -> Dom.event -> 'msg option) =
  (fun x ->
     match x with
     | EventHandlerCallback (_, cb) -> cb
     | EventHandlerMsg msg -> (fun _ev -> Some msg) : 'msg eventHandler ->
                                                        Dom.event ->
                                                          'msg option)
let compareEventHandlerTypes (left : 'msg eventHandler) =
  (fun x ->
     match x with
     | EventHandlerCallback (cb, _) ->
         (match left with
          | EventHandlerCallback (lcb, _) when cb = lcb -> true
          | _ -> false)
     | EventHandlerMsg msg ->
         (match left with
          | EventHandlerMsg lmsg when msg = lmsg -> true
          | _ -> false) : 'msg eventHandler -> bool)
let eventHandlerRegister (callbacks : 'msg applicationCallbacks ref)
  (elem : Dom.eventTarget) (name : string) (handlerType : 'msg eventHandler)
  =
  (((let cb = ref (eventHandlerGetCB handlerType) in
     let handler = eventHandler callbacks cb in
     let () = Webapi.Dom.EventTarget.addEventListener name handler elem in
     Some { handler; cb })
  [@ns.braces ]) : 'msg eventCache option)
let eventHandlerUnregister (elem : Dom.eventTarget) (name : string) =
  (fun x ->
     match x with
     | None -> None
     | Some cache ->
         let () =
           Webapi.Dom.EventTarget.removeEventListener name cache.handler elem in
         None : 'msg eventCache option -> 'msg eventCache option)
let eventHandlerMutate (callbacks : 'msg applicationCallbacks ref)
  (elem : Dom.eventTarget) (oldName : string) (newName : string)
  (oldHandlerType : 'msg eventHandler) (newHandlerType : 'msg eventHandler)
  (oldCache : 'msg eventCache option ref)
  (newCache : 'msg eventCache option ref) =
  (match oldCache.contents with
   | None ->
       newCache :=
         (eventHandlerRegister callbacks elem newName newHandlerType)
   | Some oldcache ->
       if oldName = newName
       then
         let () = newCache := oldCache.contents in
         (if compareEventHandlerTypes oldHandlerType newHandlerType
          then ()
          else
            (let cb = eventHandlerGetCB newHandlerType in
             let () = oldcache.cb := cb in ()))
       else
         (let () =
            oldCache :=
              (eventHandlerUnregister elem oldName oldCache.contents) in
          let () =
            newCache :=
              (eventHandlerRegister callbacks elem newName newHandlerType) in
          ()) : unit)
let patchVNodesOnElemsPropertiesApplyAdd
  (callbacks : 'msg applicationCallbacks ref) (elem : Dom.element)
  (_idx : int) x =
  match x with
  | NoProp -> ()
  | RawProp (k, v) -> setItem elem k v
  | Attribute (namespace, k, v) ->
      Webapi.Dom.Element.setAttributeNS namespace k v elem
  | Data (k, v) ->
      (Js.log ("TODO:  Add Data Unhandled", k, v);
       failwith "TODO:  Add Data Unhandled")
  | Event (name, handlerType, cache) ->
      let eventTarget = Webapi.Dom.Element.asEventTarget elem in
      cache := (eventHandlerRegister callbacks eventTarget name handlerType)
  | Style s ->
      (match Webapi.Dom.HtmlElement.ofElement elem with
       | Some elem ->
           let elemStyle = Webapi.Dom.HtmlElement.style elem in
           List.fold_left
             (fun () ->
                fun (k, v) ->
                  Webapi.Dom.CssStyleDeclaration.setProperty k
                    v "" elemStyle) () s
       | None ->
           failwith
             "Expected htmlelement in patchVNodesOnElems_PropertiesApplyAdd")
let patchVNodesOnElemsPropertiesApplyRemove
  (_callbacks : 'msg applicationCallbacks ref) (elem : Dom.element)
  (_idx : int) x =
  match x with
  | NoProp -> ()
  | RawProp (k, _v) -> delItem elem k
  | Attribute (namespace, k, _v) ->
      Webapi.Dom.Element.removeAttributeNS namespace k elem
  | Data (k, v) ->
      (Js.log ("TODO:  Remove Data Unhandled", k, v);
       failwith "TODO:  Remove Data Unhandled")
  | Event (name, _, cache) ->
      let eventTarget = Webapi.Dom.Element.asEventTarget elem in
      cache := (eventHandlerUnregister eventTarget name cache.contents)
  | Style s ->
      (match Webapi.Dom.HtmlElement.ofElement elem with
       | Some elem ->
           let elemStyle = Webapi.Dom.HtmlElement.style elem in
           List.fold_left
             (fun () ->
                fun (k, _v) ->
                  (Webapi.Dom.CssStyleDeclaration.removeProperty k elemStyle)
                    |> ignore) () s
       | None ->
           failwith
             "Expected htmlelement in patchVNodesOnElems_PropertiesApply_Remove")
let patchVNodesOnElemsPropertiesApplyRemoveAdd
  (callbacks : 'msg applicationCallbacks ref) (elem : Dom.element)
  (idx : int) (oldProp : 'msg property) (newProp : 'msg property) =
  (((let () =
       patchVNodesOnElemsPropertiesApplyRemove callbacks elem idx oldProp in
     let () = patchVNodesOnElemsPropertiesApplyAdd callbacks elem idx newProp in
     ())
  [@ns.braces ]) : unit)
let patchVNodesOnElemsPropertiesApplyMutate
  (_callbacks : 'msg applicationCallbacks ref) (elem : Dom.element)
  (_idx : int) (oldProp : 'msg property) x =
  match x with
  | NoProp ->
      failwith
        "This should never be called as all entries through NoProp are gated."
  | RawProp (k, v) -> setItem elem k v
  | Attribute (namespace, k, v) ->
      Webapi.Dom.Element.setAttributeNS namespace k v elem
  | Data (k, v) ->
      (Js.log ("TODO:  Mutate Data Unhandled", k, v);
       failwith "TODO:  Mutate Data Unhandled")
  | Event (_newName, _newHandlerType, _newCache) ->
      failwith "This will never be called because it is gated"
  | Style s ->
      (match Webapi.Dom.HtmlElement.ofElement elem with
       | None ->
           failwith
             "Expected htmlelement in patchVNodesOnElemsPropertiesApplyMutate"
       | Some elem ->
           let elemStyle = Webapi.Dom.HtmlElement.style elem in
           (match oldProp with
            | Style oldS ->
                List.fold_left2
                  (fun () ->
                     fun (ok, ov) ->
                       fun (nk, nv) ->
                         if ok = nk
                         then
                           (if ov = nv
                            then ()
                            else
                              Webapi.Dom.CssStyleDeclaration.setProperty
                                nk nv "" elemStyle)
                         else
                           (let (_ : string) =
                              Webapi.Dom.CssStyleDeclaration.removeProperty
                                ok elemStyle in
                            Webapi.Dom.CssStyleDeclaration.setProperty
                              nk nv "" elemStyle)) () oldS s
            | _ ->
                failwith
                  "Passed a non-Style to a new Style as a Mutations while the old Style is not actually a style!"))
let rec patchVNodesOnElemsPropertiesApply
  (callbacks : 'msg applicationCallbacks ref) (elem : Dom.element)
  (idx : int) (oldProperties : 'msg property list)
  (newProperties : 'msg property list) =
  (((match (oldProperties, newProperties) with
     | ([], []) -> true
     | ([], _newProp::_newRest) -> false
     | (_oldProp::_oldRest, []) -> false
     | ((NoProp)::oldRest, (NoProp)::newRest) ->
         patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest
           newRest
     | ((RawProp (oldK, oldV) as oldProp)::oldRest,
        (RawProp (newK, newV) as newProp)::newRest) ->
         let () =
           if (oldK = newK) && (oldV = newV)
           then ()
           else
             patchVNodesOnElemsPropertiesApplyMutate callbacks elem idx
               oldProp newProp in
         patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest
           newRest
     | ((Attribute (oldNS, oldK, oldV) as oldProp)::oldRest,
        (Attribute (newNS, newK, newV) as newProp)::newRest) ->
         let () =
           if (oldNS = newNS) && ((oldK = newK) && (oldV = newV))
           then ()
           else
             patchVNodesOnElemsPropertiesApplyMutate callbacks elem idx
               oldProp newProp in
         patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest
           newRest
     | ((Data (oldK, oldV) as oldProp)::oldRest,
        (Data (newK, newV) as newProp)::newRest) ->
         let () =
           if (oldK = newK) && (oldV = newV)
           then ()
           else
             patchVNodesOnElemsPropertiesApplyMutate callbacks elem idx
               oldProp newProp in
         patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest
           newRest
     | ((Event (oldName, oldHandlerType, oldCache) as _oldProp)::oldRest,
        (Event (newName, newHandlerType, newCache) as _newProp)::newRest) ->
         let eventTarget = Webapi.Dom.Element.asEventTarget elem in
         let () =
           eventHandlerMutate callbacks eventTarget oldName newName
             oldHandlerType newHandlerType oldCache newCache in
         patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest
           newRest
     | ((Style oldS as oldProp)::oldRest, (Style newS as newProp)::newRest)
         ->
         let () =
           if oldS = newS
           then ()
           else
             patchVNodesOnElemsPropertiesApplyMutate callbacks elem idx
               oldProp newProp in
         patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest
           newRest
     | (oldProp::oldRest, newProp::newRest) ->
         let () =
           patchVNodesOnElemsPropertiesApplyRemoveAdd callbacks elem idx
             oldProp newProp in
         patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest
           newRest)
  [@ocaml.warning "-4"]) : bool)
let patchVNodesOnElemsProperties (callbacks : 'msg applicationCallbacks ref)
  (elem : Dom.element) (oldProperties : 'msg property list)
  (newProperties : 'msg property list) =
  (patchVNodesOnElemsPropertiesApply callbacks elem 0 oldProperties
     newProperties : bool)
let genEmptyProps (length : int) =
  (((let rec aux lst x =
       match x with | 0 -> lst | len -> aux (noProp :: lst) (len - 1) in
     aux [] length)
  [@ns.braces ]) : 'msg property list)
let mapEmptyProps (props : 'msg property list) =
  (List.map (fun _ -> noProp) props : 'msg property list)
let rec patchVNodesOnElemsReplaceNode
  (callbacks : 'msg applicationCallbacks ref) (elem : Dom.node)
  (elems : Dom.nodeList) (idx : int) =
  (fun x ->
     match x with
     | Node
         (newNamespace, newTagName, _newKey, _newUnique, newProperties,
          newChildren)
         ->
         let oldChild = nodeAt idx elems in
         let newChild = createElementNsOptional newNamespace newTagName in
         let (_ : bool) =
           patchVNodesOnElemsProperties callbacks newChild
             (mapEmptyProps newProperties) newProperties in
         let newChildNode = Webapi.Dom.Element.asNode newChild in
         let childChildren = Webapi.Dom.Node.childNodes newChildNode in
         let () =
           patchVNodesOnElems callbacks newChildNode childChildren 0 []
             newChildren in
         let _attachedChild =
           Webapi.Dom.Node.insertBefore ((newChildNode)
             [@ns.namedArgLoc ]) ((oldChild)[@ns.namedArgLoc ]) elem in
         let _removedChild =
           Webapi.Dom.Node.removeChild ((oldChild)
             [@ns.namedArgLoc ]) elem in
         ()
     | _ ->
         failwith
           "Node replacement should never be passed anything but a node itself" : 
  'msg t -> unit)
and patchVNodesOnElemsCreateElement
  (callbacks : 'msg applicationCallbacks ref) =
  (fun x ->
     match x with
     | CommentNode s ->
         (Webapi.Dom.Document.createComment s Webapi.Dom.document) |>
           Webapi.Dom.Comment.asNode
     | Text text ->
         (Webapi.Dom.Document.createTextNode text Webapi.Dom.document) |>
           Webapi.Dom.Text.asNode
     | Node
         (newNamespace, newTagName, _newKey, _unique, newProperties,
          newChildren)
         ->
         let newChild = createElementNsOptional newNamespace newTagName in
         let true =
           patchVNodesOnElemsProperties callbacks newChild
             (mapEmptyProps newProperties) newProperties[@@ocaml.warning
                                                          "-8"] in
         let newChildNode = Webapi.Dom.Element.asNode newChild in
         let childChildren = Webapi.Dom.Node.childNodes newChildNode in
         let () =
           patchVNodesOnElems callbacks newChildNode childChildren 0 []
             newChildren in
         newChildNode
     | LazyGen (_newKey, newGen, newCache) ->
         let vdom = newGen () in
         let () = newCache := vdom in
         patchVNodesOnElemsCreateElement callbacks vdom
     | Tagger (tagger, vdom) ->
         patchVNodesOnElemsCreateElement (tagger callbacks) vdom : 'msg t ->
                                                                    Dom.node)
and patchVNodesOnElemsMutateNode (callbacks : 'msg applicationCallbacks ref)
  (elem : Dom.node) (elems : Dom.nodeList) (idx : int) (oldNode : 'msg t)
  (newNode : 'msg t) =
  (match (oldNode, newNode) with
   | ((Node
         (_oldNamespace, oldTagName, _oldKey, oldUnique, oldProperties,
          oldChildren)
         as _oldNode),
      (Node
         (_newNamespace, newTagName, _newKey, newUnique, newProperties,
          newChildren)
         as newNode))
       ->
       if (oldUnique <> newUnique) || (oldTagName <> newTagName)
       then patchVNodesOnElemsReplaceNode callbacks elem elems idx newNode
       else
         (let child = nodeAt idx elems in
          match Webapi.Dom.Element.ofNode child with
          | None ->
              failwith "Expected element in patchVNodesOnElems_MutateNode"
          | Some childElement ->
              let childChildren = Webapi.Dom.Node.childNodes child in
              let () =
                if
                  patchVNodesOnElemsProperties callbacks childElement
                    oldProperties newProperties
                then ()
                else
                  (let () =
                     Js.log
                       "VDom:  Failed swapping properties because the property list length changed, use `noProp` to swap properties instead, not by altering the list structure.  This is a massive inefficiency until this issue is resolved." in
                   patchVNodesOnElemsReplaceNode callbacks elem elems idx
                     newNode) in
              patchVNodesOnElems callbacks child childChildren 0 oldChildren
                newChildren)
   | _ -> failwith "Non-node passed to patchVNodesOnElemsMutateNode" : 
  unit)
and patchVNodesOnElems (callbacks : 'msg applicationCallbacks ref)
  (elem : Dom.node) (elems : Dom.nodeList) (idx : int)
  (oldVNodes : 'msg t list) (newVNodes : 'msg t list) =
  (((match (oldVNodes, newVNodes) with
     | ((Tagger (_oldTagger, oldVdom))::oldRest, _) ->
         patchVNodesOnElems callbacks elem elems idx (oldVdom :: oldRest)
           newVNodes
     | (oldNode::oldRest, (Tagger (newTagger, newVdom))::newRest) ->
         let () =
           patchVNodesOnElems (newTagger callbacks) elem elems idx [oldNode]
             [newVdom] in
         patchVNodesOnElems callbacks elem elems (idx + 1) oldRest newRest
     | ([], []) -> ()
     | ([], newNode::newRest) ->
         let newChild = patchVNodesOnElemsCreateElement callbacks newNode in
         let _attachedChild =
           Webapi.Dom.Node.appendChild ((newChild)
             [@ns.namedArgLoc ]) elem in
         patchVNodesOnElems callbacks elem elems (idx + 1) [] newRest
     | (_oldVnode::oldRest, []) ->
         let child = nodeAt idx elems in
         let _removedChild =
           Webapi.Dom.Node.removeChild ((child)
             [@ns.namedArgLoc ]) elem in
         patchVNodesOnElems callbacks elem elems idx oldRest []
     | ((CommentNode oldS)::oldRest, (CommentNode newS)::newRest) when
         oldS = newS ->
         patchVNodesOnElems callbacks elem elems (idx + 1) oldRest newRest
     | ((Text oldText)::oldRest, (Text newText)::newRest) ->
         let () =
           if oldText = newText
           then ()
           else
             (let child = nodeAt idx elems in
              Webapi.Dom.Node.setNodeValue child (Js.Null.return newText)) in
         patchVNodesOnElems callbacks elem elems (idx + 1) oldRest newRest
     | ((LazyGen (oldKey, _oldGen, oldCache))::oldRest, (LazyGen
        (newKey, newGen, newCache))::newRest) ->
         if oldKey = newKey
         then
           let () = newCache := oldCache.contents in
           patchVNodesOnElems callbacks elem elems (idx + 1) oldRest newRest
         else
           (match (oldRest, newRest) with
            | ((LazyGen (olderKey, _olderGen, _olderCache))::olderRest,
               (LazyGen (newerKey, _newerGen, _newerCache))::newerRest) when
                (olderKey = newKey) && (oldKey = newerKey) ->
                let firstChild = nodeAt idx elems in
                let secondChild = nodeAt (idx + 1) elems in
                let _removedChild =
                  Webapi.Dom.Node.removeChild ((secondChild)
                    [@ns.namedArgLoc ]) elem in
                let _attachedChild =
                  Webapi.Dom.Node.insertBefore ((secondChild)
                    [@ns.namedArgLoc ]) ((firstChild)
                    [@ns.namedArgLoc ]) elem in
                patchVNodesOnElems callbacks elem elems (idx + 2) olderRest
                  newerRest
            | ((LazyGen (olderKey, _olderGen, olderCache))::olderRest, _)
                when olderKey = newKey ->
                let oldChild = nodeAt idx elems in
                let _removedChild =
                  Webapi.Dom.Node.removeChild ((oldChild)
                    [@ns.namedArgLoc ]) elem in
                let oldVdom = olderCache.contents in
                let () = newCache := oldVdom in
                patchVNodesOnElems callbacks elem elems (idx + 1) olderRest
                  newRest
            | (_, (LazyGen (newerKey, _newerGen, _newerCache))::_newerRest)
                when newerKey = oldKey ->
                let oldChild = nodeAt idx elems in
                let newVdom = newGen () in
                let () = newCache := newVdom in
                let newChild =
                  patchVNodesOnElemsCreateElement callbacks newVdom in
                let _attachedChild =
                  Webapi.Dom.Node.insertBefore ((newChild)
                    [@ns.namedArgLoc ]) ((oldChild)
                    [@ns.namedArgLoc ]) elem in
                patchVNodesOnElems callbacks elem elems (idx + 1) oldVNodes
                  newRest
            | _ ->
                let oldVdom = oldCache.contents in
                let newVdom = newGen () in
                let () = newCache := newVdom in
                patchVNodesOnElems callbacks elem elems idx (oldVdom ::
                  oldRest) (newVdom :: newRest))
     | ((Node
           (oldNamespace, oldTagName, oldKey, _oldUnique, _oldProperties,
            _oldChildren)
           as oldNode)::oldRest,
        (Node
           (newNamespace, newTagName, newKey, _newUnique, _newProperties,
            _newChildren)
           as newNode)::newRest)
         ->
         if (oldKey = newKey) && (oldKey <> "")
         then
           patchVNodesOnElems callbacks elem elems (idx + 1) oldRest newRest
         else
           if (oldKey = "") || (newKey = "")
           then
             (let () =
                patchVNodesOnElemsMutateNode callbacks elem elems idx oldNode
                  newNode in
              patchVNodesOnElems callbacks elem elems (idx + 1) oldRest
                newRest)
           else
             (match (oldRest, newRest) with
              | ((Node
                 (olderNamespace, olderTagName, olderKey, _olderUnique,
                  _olderProperties, _olderChildren))::olderRest,
                 (Node
                 (newerNamespace, newerTagName, newerKey, _newerUnique,
                  _newerProperties, _newerChildren))::newerRest) when
                  (olderNamespace = newNamespace) &&
                    ((olderTagName = newTagName) &&
                       ((olderKey = newKey) &&
                          ((oldNamespace = newerNamespace) &&
                             ((oldTagName = newerTagName) &&
                                (oldKey = newerKey)))))
                  ->
                  let firstChild = nodeAt idx elems in
                  let secondChild = nodeAt (idx + 1) elems in
                  let _removedChild =
                    Webapi.Dom.Node.removeChild ((secondChild)
                      [@ns.namedArgLoc ]) elem in
                  let _attachedChild =
                    Webapi.Dom.Node.insertBefore ((secondChild)
                      [@ns.namedArgLoc ]) ((firstChild)
                      [@ns.namedArgLoc ]) elem in
                  patchVNodesOnElems callbacks elem elems (idx + 2) olderRest
                    newerRest
              | ((Node
                 (olderNamespace, olderTagName, olderKey, _olderUnique,
                  _olderProperties, _olderChildren))::olderRest,
                 _) when
                  (olderNamespace = newNamespace) &&
                    ((olderTagName = newTagName) && (olderKey = newKey))
                  ->
                  let oldChild = nodeAt idx elems in
                  let _removedChild =
                    Webapi.Dom.Node.removeChild ((oldChild)
                      [@ns.namedArgLoc ]) elem in
                  patchVNodesOnElems callbacks elem elems (idx + 1) olderRest
                    newRest
              | (_, (Node
                 (newerNamespace, newerTagName, newerKey, _newerUnique,
                  _newerProperties, _newerChildren))::_newerRest) when
                  (oldNamespace = newerNamespace) &&
                    ((oldTagName = newerTagName) && (oldKey = newerKey))
                  ->
                  let oldChild = nodeAt idx elems in
                  let newChild =
                    patchVNodesOnElemsCreateElement callbacks newNode in
                  let _attachedChild =
                    Webapi.Dom.Node.insertBefore ((newChild)
                      [@ns.namedArgLoc ]) ((oldChild)
                      [@ns.namedArgLoc ]) elem in
                  patchVNodesOnElems callbacks elem elems (idx + 1) oldVNodes
                    newRest
              | _ ->
                  let () =
                    patchVNodesOnElemsMutateNode callbacks elem elems idx
                      oldNode newNode in
                  patchVNodesOnElems callbacks elem elems (idx + 1) oldRest
                    newRest)
     | (_oldVnode::oldRest, newNode::newRest) ->
         let oldChild = nodeAt idx elems in
         let newChild = patchVNodesOnElemsCreateElement callbacks newNode in
         let _attachedChild =
           Webapi.Dom.Node.insertBefore ((newChild)
             [@ns.namedArgLoc ]) ((oldChild)[@ns.namedArgLoc ]) elem in
         let _removedChild =
           Webapi.Dom.Node.removeChild ((oldChild)
             [@ns.namedArgLoc ]) elem in
         patchVNodesOnElems callbacks elem elems (idx + 1) oldRest newRest)
  [@ocaml.warning "-4"]) : unit)
let patchVNodesIntoElement (callbacks : 'msg applicationCallbacks ref)
  (elem : Dom.node) (oldVNodes : 'msg t list) (newVNodes : 'msg t list) =
  (((let elems = Webapi.Dom.Node.childNodes elem in
     let () = patchVNodesOnElems callbacks elem elems 0 oldVNodes newVNodes in
     newVNodes)
  [@ns.braces ]) : 'msg t list)
let patchVNodeIntoElement (callbacks : 'msg applicationCallbacks ref)
  (elem : Dom.node) (oldVNode : 'msg t) (newVNode : 'msg t) =
  (patchVNodesIntoElement callbacks elem [oldVNode] [newVNode] : 'msg t list)
let wrapCallbacksOn : type a b.
  (a -> b) -> a systemMessage -> b systemMessage =
  fun func ->
    fun x ->
      match x with
      | Render -> Render
      | AddRenderMsg msg -> AddRenderMsg (func msg)
      | RemoveRenderMsg msg -> RemoveRenderMsg (func msg)
let wrapCallbacks : type a b.
  (a -> b) -> b applicationCallbacks ref -> a applicationCallbacks ref =
  fun func ->
    fun callbacks ->
      Obj.magic ref
        {
          enqueue =
            (fun (msg : a) ->
               ((let new_msg = func msg in
                 (callbacks.contents).enqueue new_msg)
               [@ns.braces ]));
          on =
            (fun smsg ->
               ((let new_smsg = wrapCallbacksOn func smsg in
                 (callbacks.contents).on new_smsg)
               [@ns.braces ]))
        }
let (map : ('a -> 'b) -> 'a t -> 'b t) =
  (fun func ->
     fun vdom ->
       ((let tagger = wrapCallbacks func in
         Tagger ((Obj.magic tagger), (Obj.magic vdom)))
       [@ns.braces ]) : ('a -> 'b) -> 'a t -> 'b t)
