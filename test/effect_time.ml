open Tea

type nonrec time = float

type nonrec 'msg mySub = Every of time * (time -> 'msg)

type nonrec 'msg myCmd = Delay of time * (unit -> 'msg)

let every interval tagger = Every (interval, tagger)

let delay msTime msg =
  Cmd.call (fun callbacks ->
      (let _unhandledID =
         Js.Global.setTimeout
           (fun () ->
             ((let open Vdom in
               callbacks.contents.enqueue msg ) [@ns.braces] ) )
           msTime
       in
       () )
      [@ns.braces] )
