open Tea.Html
open Tea.Html.Events

type nonrec msg = Increment | Decrement | Reset | Set of int

type nonrec model = int

let init () = 4

let update model x =
  match x with
  | Increment ->
      model + 1
  | Decrement ->
      model - 1
  | Reset ->
      0
  | Set v ->
      v

let view_button title msg = button [onClick msg] [text title]

let view lift model =
  div
    [ Tea.Html.Attributes.styles
        [("display", "inline-block"); ("vertical-align", "top")] ]
    [ span
        [Tea.Html.Attributes.style "text-weight" "bold"]
        [text (string_of_int model)]
    ; br []
    ; view_button "Increment" (lift Increment)
    ; br []
    ; view_button "Decrement" (lift Decrement)
    ; br []
    ; view_button "Set to 42" (lift (Set 42))
    ; br []
    ; (if model <> 0 then view_button "Reset" (lift Reset) else noNode) ]
