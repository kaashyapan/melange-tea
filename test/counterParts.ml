open Tea
open Tea_html
open Tea.Html.Events

type nonrec msg =
  | Increment of int
  | Decrement of int
  | Reset of int
  | Set of int * int
  | Shutdown of int

module IntMap = Map.Make (struct
  type nonrec t = int

  let compare = compare
end)

type nonrec 'parentMsg model =
  {values: int IntMap.t; defaultValue: int; lift: msg -> 'parentMsg}

let init defaultValue lift = {values= IntMap.empty; defaultValue; lift}

let getValue id model =
  if IntMap.mem id model.values then IntMap.find id model.values
  else model.defaultValue

let putValue id value model =
  {model with values= IntMap.add id value model.values}

let mutateValue id op model =
  (let value = getValue id model in
   putValue id (op value) model )
  [@ns.braces]

let remove_value id model = {model with values= IntMap.remove id model.values}

let update model x =
  match x with
  | Increment id ->
      mutateValue id (( + ) 1) model
  | Decrement id ->
      mutateValue id (fun i -> i - 1) model
  | Reset id ->
      putValue id 0 model
  | Set (id, v) ->
      putValue id v model
  | Shutdown id ->
      remove_value id model

let shutdown model id = Cmd.msg (model.lift (Shutdown id))

let viewButton title msg = button [onClick msg] [text title]

let view id model =
  (let lift = model.lift in
   let value = getValue id model in
   div
     [ Tea.Html.Attributes.styles
         [("display", "inline-block"); ("vertical-align", "top")] ]
     [ span
         [Tea.Html.Attributes.style "text-weight" "bold"]
         [text (Belt.Int.toString value)]
     ; br []
     ; viewButton "Increment" (lift (Increment id))
     ; br []
     ; viewButton "Decrement" (lift (Decrement id))
     ; br []
     ; viewButton "Set to 42" (lift (Set (id, 42)))
     ; br []
     ; (if value <> 0 then viewButton "Reset" (lift (Reset id)) else noNode) ]
  )
  [@ns.braces]
