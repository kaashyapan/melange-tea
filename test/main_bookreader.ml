open Tea
open Tea.Html.Events
module Progress = Http.Progress

type nonrec model =
  {progress: Progress.t; bookUrl: string option; bookContent: string}

let initModel =
  {progress= Progress.emptyProgress; bookUrl= None; bookContent= ""}

let init () = (initModel, Cmd.none)

type nonrec msg =
  | NoOp
  | GetBook of string
  | GetBookProgress of string * Progress.t
  | GetBookDone of string * string

let subscriptions _model = Sub.none

let progressHelper b =
  ((let open Progress in
    {bytes= b; bytesExpected= 1} ) [@ns.braces] )

let update model x =
  match x with
  | NoOp ->
      (model, Cmd.none)
  | GetBook url ->
      let httpCmd =
        Http.getString url
        |> Http.Progress.track (fun progress ->
               GetBookProgress (url, progress) )
        |> Http.send (fun x ->
               match x with
               | Error _e ->
                   NoOp
               | Ok output ->
                   GetBookDone (url, output) )
      in
      ({model with bookUrl= Some url; progress= progressHelper 0}, httpCmd)
  | GetBookProgress (url, progress) ->
      if Some url <> model.bookUrl then (model, Cmd.none)
      else ({model with progress}, Cmd.none)
  | GetBookDone (url, bookContent) ->
      if Some url <> model.bookUrl then (model, Cmd.none)
      else ({model with bookContent; progress= progressHelper 1}, Cmd.none)

let viewStyle =
  ((let open Tea.Html.Attributes in
    styles
      [ ("display", "flex")
      ; ("flex-direction", "column")
      ; ("width", "100%")
      ; ("margin", "0 auto")
      ; ("font-family", "Arial") ] ) [@ns.braces] )

let bookTextViewStyle =
  ((let open Html.Attributes in
    styles [("height", "400px"); ("width", "100%")] ) [@ns.braces] )

let inputRadio labelText url =
  ((let open Tea_html in
    let open Tea_html.Attributes in
    div []
      [ label
          [onCheck (fun isChecked -> if isChecked then GetBook url else NoOp)]
          [input' [type' "radio"; name "book-radio"] []; text labelText] ] )
  [@ns.braces] )

let progressView loaded =
  ((let open Tea.Html in
    let open Tea.Html.Attributes in
    div []
      [ span [] [text "Progress: "]
      ; progress [value loaded; Attributes.max "100"] [text (loaded ^ "%")]
      ; text (loaded ^ "%") ] ) [@ns.braces] )

let progressLoaded progress =
  ((let open Progress in
    let bytes = float_of_int progress.bytes in
    let bytesExpected = float_of_int progress.bytesExpected in
    if bytesExpected <= 0.0 then 100
    else int_of_float (100.0 *. (bytes /. bytesExpected)) ) [@ns.braces] )

let footerView =
  ((let open Tea.Html in
    let open Tea.Html.Attributes in
    span []
      [ text "Books from "
      ; a
          [href "http://www.gutenberg.org/"; target "_blank"]
          [text "Project Gutenberg"] ] ) [@ns.braces] )

let bookTextView valueText =
  ((let open Tea.Html in
    let open Tea.Html.Attributes in
    div []
      [ textarea
          [value valueText; Attributes.disabled true; bookTextViewStyle]
          [] ] ) [@ns.braces] )

let view model =
  ((let open Html in
    div [viewStyle]
      [ h1 [] [text "Book Reader"]
      ; p []
          [ text "Select a book:"
          ; inputRadio "Essays - Ralph Waldo Emerson"
              "https://s3-sa-east-1.amazonaws.com/estadistas/Essays-Ralph-Waldo-Emerson.txt"
          ; inputRadio "Leviathan - Thomas Hobbes"
              "https://s3-sa-east-1.amazonaws.com/estadistas/Leviathan.txt"
          ; inputRadio "The Ethics of Aristotle - Aristotle"
              "https://s3-sa-east-1.amazonaws.com/estadistas/The-Ethics-of+Aristotle.txt"
          ]
      ; progressView (Belt.Int.toString (progressLoaded model.progress))
      ; bookTextView model.bookContent
      ; footerView ] ) [@ns.braces] )

let main =
  ((let open App in
    standardProgram {init; update; view; subscriptions} ) [@ns.braces] )
