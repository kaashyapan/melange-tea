open Tea.App
open Tea.Html
open Tea.Html.Attributes
open Tea.Html.Events

type nonrec model = {name: string; password: string; passwordAgain: string}

let model = {name= ""; password= ""; passwordAgain= ""}

type nonrec msg =
  | Name of string
  | Password of string
  | PasswordAgain of string

let update model x =
  match x with
  | Name name ->
      {model with name}
  | Password password ->
      {model with password}
  | PasswordAgain passwordAgain ->
      {model with passwordAgain}

let viewValidation model =
  (let color, message =
     if model.password == model.passwordAgain then ("green", "OK")
     else ("red", "Passwords do not match!")
   in
   div [styles [("color", color)]] [text message] )
  [@ns.braces]

let view model =
  div []
    [ input' [type' "text"; placeholder "Name"; onInput (fun s -> Name s)] []
    ; input'
        [type' "password"; placeholder "Password"; onInput (fun s -> Password s)]
        []
    ; input'
        [ type' "password"
        ; placeholder "Re-enter Password"
        ; onInput (fun s -> PasswordAgain s) ]
        []
    ; viewValidation model ]

let main = beginnerProgram {model; view; update}
