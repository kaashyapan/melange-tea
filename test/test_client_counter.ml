open Tea.App
open Tea.Html
open Tea_html.Events

type nonrec msg = Increment | Decrement | Reset | Set of int

let update model x =
  match x with
  | Increment ->
      model + 1
  | Decrement ->
      model - 1
  | Reset ->
      0
  | Set v ->
      v

let view_button title msg = button [onClick msg] [text title]

let view model =
  div []
    [ span
        [Tea_html.Attributes.style "text-weight" "bold"]
        [text (Belt.Int.toString model)]
    ; br []
    ; view_button "Increment" (if model >= 3 then Decrement else Increment)
    ; br []
    ; view_button "Decrement" Decrement
    ; br []
    ; view_button "Set to 42" (Set 42)
    ; br []
    ; (if model <> 0 then view_button "Reset" Reset else noNode) ]

let main = beginnerProgram {model= 4; update; view}
