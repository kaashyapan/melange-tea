open Tea.App
open Tea.Html
open Tea.Html.Events

type nonrec msg = Counter of int * Counter.msg | AddCounter | RemoveCounter

type nonrec model = {counters: Counter.model list}

let update model x =
  match x with
  | Counter (idx, ms) ->
      let () = Js.log (model, idx, ms) in
      { counters=
          model.counters
          |> List.mapi (fun i m -> if i <> idx then m else Counter.update m ms)
      }
  | AddCounter ->
      {counters= Counter.init () :: model.counters}
  | RemoveCounter ->
      {counters= List.tl model.counters}

let view_button title msg = button [onClick msg] [text title]

let view model =
  div []
    [ button [onClick AddCounter] [text "Prepend a Counter"]
    ; ( if List.length model.counters = 0 then noNode
        else button [onClick RemoveCounter] [text "Delete a Counter"] )
    ; div []
        (List.mapi
           (fun i mo -> Counter.view (fun ms -> Counter (i, ms)) mo)
           model.counters ) ]

let main = beginnerProgram {model= {counters= []}; update; view}
