open Tea.App
open Tea_html.Events
open Tea_html.Attributes
open! Tea.Html

type nonrec msg = Trigger [@@deriving accessors]

type nonrec model = string option * string option

let update' model x =
  match x with
  | Trigger ->
      let left, _ = model in
      (left, Some "right")

let render_model x =
  match x with
  | Some _, Some _ ->
      input' [value "This should be on screen"] []
  | _ ->
      span [] [text "nothing"]

let view' model =
  div [] [button [onClick Trigger] [text "trigger rerender"]; render_model model]

let main =
  beginnerProgram {model= (Some "left", None); update= update'; view= view'}
