open Tea
open App
open Html
open Tea_html.Events

type nonrec model = {r: int; g: int; b: int}

type nonrec msg = Roll | NewColor of int * int * int

let init () = ({r= 255; g= 255; b= 255}, Cmd.none)

let update model x =
  match x with
  | Roll ->
      let genInt = Tea.Random.int 1 255 in
      let genTuple3 =
        Tea.Random.map3 (fun r g b -> NewColor (r, g, b)) genInt genInt genInt
      in
      (model, Tea.Random.generate (fun v -> v) genTuple3)
  | NewColor (r, g, b) ->
      ({r; g; b}, Cmd.none)

let subscriptions _model = Sub.none

let view model =
  (let r = Belt.Int.toString model.r in
   let g = Belt.Int.toString model.g in
   let b = Belt.Int.toString model.b in
   let isDark = (model.r + model.g + model.b) / 3 > 128 in
   let rgb = "(" ^ r ^ "," ^ g ^ "," ^ b ^ ")" in
   let altRgb = if isDark then "(0,0,0)" else "(255,255,255)" in
   div []
     [ h1
         [ Tea_html.Attributes.style "background-color" ("rgb" ^ rgb)
         ; Tea_html.Attributes.style "color" ("rgb" ^ altRgb) ]
         [text rgb]
     ; button [onClick Roll] [text "Roll"] ] )
  [@ns.braces]

let main = standardProgram {init; update; view; subscriptions}
