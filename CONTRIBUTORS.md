# Contributors

In alphabetical order in their requisite sections.

## Contributors to melange-tea

- @pdelacroix

## Contributors to rescript-tea

- @ianconnolly (Ian Connolly): Adding type signatures, bug fixing, code cleanup.

- @OceanOak (Ocean Oak): Converting to modern Rescript

- @pbiggar (Paul Biggar)

## Contributors to bucklescript-tea

- @alfredfriedrich (Peter Klett): Adding further to the API.

- @BenSchZA (Benjamin Scholtz): Documenting method to integrate BS-TEA into ReasonReact components.

- @canadaduane (Duane Johnson): Creating the auto-conversion between the OCaml and Rescript files, tremendously useful so people can see how each work!

- @dboris (Boris Dobroslavov): Improving model history debug format.

- @feluxe: Starter-kit provider.

- @IwanKaramazow: Bug finder extraordinaire, thanks for catching all those corner cases! Also helping flesh out the API.

- @jackalcooper: Bug finder extraordinaire, thanks for catching those Rescript issues! Also helped flesh out the API.

- @jordwest: Adding further to the API.

- @neochrome (Johan Stenqvist): Adding further to the API.

- @overminddl1 (Gabriel Robertson): Original author.

- @pbiggar (Paul Biggar): Adding type signatures, bug fixing, code cleanup.

- @seadynamic8: Better matching to the base API, I.E. Bug Fixing.

- @soren-n: Adding further to the API.

- @tcoopman: Adding further to the API.

- @utkarshkukreti: Bug finder extraordinaire!

## Special thanks

- @anmonteiro (Antonio Nuno Monteiro): Creating Melange.

- @bobzhang (Hongbo Zhang): Creating Bucklescript and Rescript.

- @evancz (Evan Czaplicki): Creating The Elm Architecture.
